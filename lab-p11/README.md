# Lab-P11: Scatter Plots and Recursion

In this lab, you'll get practice with creating scatter plots and defining recursive functions, in preparation for P11.

**If you can't open `expected_plots.json` on Gitlab, scroll down to [Segment 1: Setup](#segment-1-setup) to download from the given link**.

-----------------------------
## Corrections/Clarifications

- Updated `sample_data.zip`. People who downloaded this lab before 8:30am Wednesday (Apr 10) should re-download the zip file, otherwise Q19 will fail.
- For Q10, there was originally a typo that asks to plot `transit_planet_masses` but actually refers to `transit_planet_masses_no_outliers`. It has been fixed.

**Find any issues?** Please report to us:

- Yuheng Wu <yuheng.wu@wisc.edu>

------------------------------
## Learning Objectives

In this lab, you will practice...
* creating scatter plots,
* defining recursive functions.

------------------------------

## Note on Academic Misconduct

You may do these lab exercises with only your project partner; you are not allowed to start
working on Lab-P11 with one person, then do the project with a different partner. Now may be a
good time to review [our course policies](https://cs220.cs.wisc.edu/s24/syllabus.html).

**Important:** P10 and P11 are two parts of the same data analysis.
You **cannot** switch project partners between these two projects.
If you partnered up with someone for P10, you have to sustain that partnership until the end of P11.

------------------------------

<h2 id="setup">Segment 1: Setup</h2>

Create a `lab-p11` directory and download the following files into the `lab-p11` directory.

* `sample_data.zip`
* `planets_small.json`
* `lab-p11.ipynb`
* `public_tests.py`
* `expected_plots.json`

If it takes too long to load the file `expected_plots.json` on GitLab, you can directly download the file from this links: [`expected_plots.json`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/raw/main/lab-p11/expected_plots.json). You will need to **Right Click**, and click on the **Save as...** button to save the file through this method.

After downloading `sample_data.zip`, make sure to extract it (using [Mac directions](http://osxdaily.com/2017/11/05/how-open-zip-file-mac/) or [Windows directions](https://support.microsoft.com/en-us/help/4028088/windows-zip-and-unzip-files)). After extracting, you should see a folder called `sample_data`. You may delete `sample_data.zip` after extracting the files inside it.

**Important**: You **must** make sure that the `sample_data` directory is in the same directory as `practice.ipynb`. Your file structure **must** look like this:

```
+-- lab-p11.ipynb
+-- planets_small.json
+-- public_tests.py
+-- expected_plots.json
+-- sample_data
|   +-- .DS_Store
|   +-- file_1.json
|   +-- sample_1
|   |   +-- .ipynb_checkpoints
|   |   +-- file_2.json
|   |   +-- file_3.json
|   +-- sample_2
|   |   +-- file_4.json
|   |   +-- sample_3
|   |   |   +-- .DS_Store
|   |   |   +-- file_5.json
```


## Segment 2:
For the remaining segments, detailed instructions are provided in `lab-p11.ipynb`. From the terminal, open a `jupyter notebook` session, open your `lab-p11.ipynb`, and follow the instructions in `lab-p11.ipynb`.

## Project 11

You can now get started with [P11](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/p11). **You may copy/paste any code created here in project P11**. Remember to work on P11 with only your partner from this point on. Have fun!
