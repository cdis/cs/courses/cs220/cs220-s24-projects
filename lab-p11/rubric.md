# Lab Project 11 (Lab-P11) Grading Rubric


## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- You will receive full points if you pass the public tests associated with each question, and you will not receive any points for a question if you fail the public tests of that question.

## Rubric

### Question specific guidelines:

- q1 (4)

- q2 (4)

- q3 (4)

- q4 (4)

- q5 (4)

- q6 (4)

- q7 (4)

- q8 (4)

- q9 (4)

- q11 (4)

- q12 (5)

- q13 (5)

- q14 (5)

- q15 (5)

- q16 (5)

- q17 (5)

- q18 (5)

- q19 (5)

- q20 (5)

- q21 (5)

- q22 (5)

- q23 (5)

