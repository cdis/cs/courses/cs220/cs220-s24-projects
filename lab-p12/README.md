# Lab 12: Web Requests, Caching, DataFrames and Scraping

In this lab, you'll get practice with downloading files from the web, analyzing data using `pandas`, and parsing data in HTML files.

-----------------------------
## Corrections/Clarifications


**Find any issues?** Please report to us:

- Yuheng Wu <yuheng.wu@wisc.edu>

## Learning Objectives:

In this lab, you will practice how to:

* use HTTP requests to download content from the internet,
* cache data onto your computer,
* construct and modify DataFrames to analyze datasets,
* use `BeautifulSoup` to parse web pages and extract useful information.

------------------------------

## Note on Academic Misconduct

You may do these lab exercises only with your project partner; you are not allowed to start
working on Lab-P12 with one person, then do the project with a different partner. Now may be a
good time to review [our course policies](https://cs220.cs.wisc.edu/s24/syllabus.html).

**Important:** P12 and P13 are two parts of the same data analysis.
You **cannot** switch project partners between these two projects.
If you partnered up with someone for P12, you have to sustain that partnership until the end of P13.

------------------------------

## Segment 0: Setup

Create a `lab-p12` directory and download the following files into the `lab-p12` directory.

* [`lab-p12.ipynb`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/lab-p12/lab-p12.ipynb)
* [`public_tests.py`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/blob/main/lab-p12/public_tests.py)
* [`expected_dfs.html`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/blob/main/lab-p12/expected_dfs.html)

## Segments 1-3: Web Requests, Caching, DataFrames and Scraping

For the remaining segments, detailed instructions are provided in `lab-p12.ipynb`. From the terminal, open a `jupyter notebook` session, open your `lab-p12.ipynb` and follow the instructions in `lab-p12.ipynb`.

## Project 12

You can now get started with [P12](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/p12). **You may copy/paste any code created here in project P12**. Remember to only work on P12 with your partner from this point on. Have fun!
