# Lab Project 12 (Lab-P12) Grading Rubric


## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- You will receive full points if you pass the public tests associated with each question, and you will not receive any points for a question if you fail the public tests of that question.

## Rubric

### Question specific guidelines:

- q1 (5)

- q2 (5)

- q3 (6)

- q4 (6)

- q5 (6)

- q6 (6)

- q7 (6)

- q8 (6)

- q9 (6)

- q10 (6)

- q11 (6)

- q12 (6)

- q13 (6)

- q14 (6)

- q15 (6)

- q16 (6)

- q17 (6)

- q18 (0)

- q19 (0)

- q20 (0)

