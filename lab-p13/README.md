# Lab-P13: SQL Databases and Plotting

In this lab, you'll learn to use SQL queries to extract data from a database. You will also write various plotting functions to visualize the extracted data.

-----------------------------
## Corrections/Clarifications


**Find any issues?** Please report to us:

- Yuheng Wu <yuheng.wu@wisc.edu>

## Learning Objectives:

In this lab, you will practice how to:

* use SQL queries to extract data from a database,
* use different SQL keywords to organize data,
* write different plotting functions to visualize data and engage in data exploration.

------------------------------

## Note on Academic Misconduct

You may do these lab exercises only with your project partner; you are not allowed to start
working on Lab-P13 with one person, then do the project with a different partner. Now may be a
good time to review [our course policies](https://cs220.cs.wisc.edu/s24/syllabus.html).

**Important:** P12 and P13 are two parts of the same data analysis.
You **cannot** switch project partners between these two projects.
If you partnered up with someone for P12, you have to sustain that partnership until the end of P13.

------------------------------

## Segment 0: Setup

Create a `lab-p13` directory and download the following files into the `lab-p13` directory.

* [`lab-p13.ipynb`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/lab-p13/lab-p13.ipynb)
* [`public_tests.py`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/blob/main/lab-p13/public_tests.py)
* [`expected_dfs.html`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/blob/main/lab-p13/expected_dfs.html)

## Segments 1-3: Web Requests, Caching, DataFrames and Scraping

For the remaining segments, detailed instructions are provided in `lab-p13.ipynb`. From the terminal, open a `jupyter notebook` session, open your `lab-p13.ipynb` and follow the instructions in `lab-p13.ipynb`.

## Project 13

You can now get started with [P13](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/p13). **You may copy/paste any code created here in project P13**. Remember to only work on P13 with your partner from this point on. Have fun!
