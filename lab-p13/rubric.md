# Lab Project 13 (Lab-P13) Grading Rubric


## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- You will receive full points if you pass the public tests associated with each question, and you will not receive any points for a question if you fail the public tests of that question.

## Rubric

### Question specific guidelines:

- q1 (7)

- q2 (7)

- q3 (7)

- q4 (7)

- q5 (7)

- q6 (7)

- q7 (7)

- q8 (7)

- q9 (7)

- q10 (7)

- q11 (7)

- q12 (7)

- q13 (7)

- q14 (9)

