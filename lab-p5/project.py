__steam__ = []

# rows: 
# - name
# - publisher
# - release_date
# - avg_playtime
# - price
# - positive_reviews
# - negative_reviews


def __init__():
    import csv
    """This function will read in the csv_file and store it in a list of dictionaries"""
    __steam__.clear()
    with open('steam.csv', mode='r', encoding='utf-8') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            __steam__.append(row)


def count():
    """This function will return the number of records in the dataset (i.e., number of games)"""
    return len(__steam__)


def get_name(idx):
    """get_name(idx) returns the name of the game in row idx"""
    return __steam__[int(idx)]['name']


def get_publisher(idx):
    """get_publisher(idx) returns the publisher of the game in row idx"""
    return __steam__[int(idx)]['publisher']

def get_release_date(idx):
    """
    get_release_date(idx) returns the release date of the game in row idx
    format: mm/dd/yyyy
    """
    return __steam__[int(idx)]['release_date']

def get_avg_playtime(idx):
    """get_avg_playtime(idx) returns the average playtime (in minutes) of the game in row idx"""
    return int(__steam__[int(idx)]['avg_playtime'])

def get_price(idx):
    """get_price(idx) returns the price of the game in row idx"""
    return __steam__[int(idx)]['price']

def get_positive_reviews(idx):
    """get_positive_reviews(idx) returns the number of positive reviews of the game in row idx"""
    return __steam__[int(idx)]['positive_reviews']

def get_negative_reviews(idx):
    """get_negative_reviews(idx) returns the number of negative reviews of the game in row idx"""
    return __steam__[int(idx)]['negative_reviews']


__init__()