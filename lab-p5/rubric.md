# Lab Project 5 (Lab-P5) Grading Rubric


## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- You will receive full points if you pass the public tests associated with each question, and you will not receive any points for a question if you fail the public tests of that question.

## Rubric

### Question specific guidelines:

- q1 (2)

- q2 (2)

- q3 (2)

- q4 (2)

- q5 (2)

- q6 (2)

- q7 (2)

- q8 (2)

- q9 (3)

- q10 (3)

- q11 (3)

- q12 (3)

- q13 (3)

- q14 (3)

- q15 (3)

- q16 (3)

- q17 (3)

- q18 (3)

- q19 (3)

- q20 (3)

- q21 (3)

- q22 (3)

- q23 (3)

- q24 (3)

- q25 (3)

- q26 (3)

- q27 (3)

- q28 (3)

- q29 (3)

- q30 (3)

- q31 (3)

- q32 (3)

- q33 (3)

- q34 (3)

- q35 (3)

- q36 (3)

