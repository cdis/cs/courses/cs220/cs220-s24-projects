# Lab P6: Reading CSV Data about Power Generators

In this lab, you will practice accessing CSVs, sorting, and using sets. You will work with a real-world dataset and deal with issues that arise when using such datasets.

### Corrections/Clarifications

None yet

**Find any issues?** Report to us:

- Yuheng Wu <yuheng.wu@wisc.edu>

------------------------------
## Learning Objectives

In this lab, you will practice:
* accessing data directly from a CSV file,
* dealing with messy real-world datasets,
* using string functions to compare and extract data values,
* using sorting functions to order data,
* using sets to determine the unique values in a list

------------------------------
## Note on Academic Misconduct

You may do these lab exercises only with your project partner; you are not allowed to start working on Lab-P6 with one person, then do the project with a different partner.  Now may be a good time to review [our course policies](https://cs220.cs.wisc.edu/s24/syllabus.html).


------------------------------
## Segment 1: Setup

Create a `lab-p6` directory and download the following files into the `lab-p6` directory.

* `power_generators.csv`
* `lab-p6.ipynb`
* `public_tests.py`

**Note:** If you accidentally downloaded the file as a `.txt` instead of `.csv` (or `.cvs` or `.csv.txt`)
(say `power_generators.cvs`), you can execute `mv power_generators.cvs power_generators.csv` on a
Terminal/PowerShell window. Recall that the `mv` (move) command lets you rename a source file
(first argument, example: `power_generators.cvs`) to the destination file (second argument, example:
`power_generators.csv`).

**Warning:** Please make sure that the name of the file is `power_generators.csv` in your local computer. If your file has a different name on your computer, you may pass the *local tests*, but **fail** the tests on Gradescope.

To start, familiarize yourself with the dataset (`power_generators.csv`). Examine its contents using Microsoft Excel, Numbers (Mac) or any other spreadsheet viewing software.

------------------------------

## Segment 2: Working with `power_generators.csv`

You will be finishing the rest of your lab on `lab-p6.ipynb`. Run the command `jupyter notebook` from your Terminal/PowerShell window.
Remember not to close this
Terminal/PowerShell window while Jupyter is running, and open a new Terminal/PowerShell
window if necessary.

**Note**: For P6, you will be working on `p6.ipynb`, which is very similar to `lab-p6.ipynb`. We
strongly recommend that you finish working on this notebook during the lab, so you can ask
your TA/PM any questions about the notebook that may arise.

**Note**: Unlike `p6.ipynb`, you do **not** have to submit `lab-p6.ipynb`. This notebook is solely
for your practice and preparation for P6.

------------------------------

You can now get started with [P6]((https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/p6)). **You may use any helper functions created here in project P6**. Remember to only work with P6 with your partner from this point on. Have fun!
