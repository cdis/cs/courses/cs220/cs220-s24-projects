# Lab Project 6 (Lab-P6) Grading Rubric


## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- You will receive full points if you pass the public tests associated with each question, and you will not receive any points for a question if you fail the public tests of that question.

## Rubric

### Question specific guidelines:

- q1 (3)

- q2 (3)

- q3 (3)

- q4 (3)

- q5 (3)

- q6 (3)

- q7 (3)

- q8 (3)

- q9 (3)

- q10 (3)

- q11 (3)

- q12 (3)

- q13 (3)

- q14 (3)

- q15 (3)

- q16 (3)

- q17 (3)

- q18 (3)

- q19 (3)

- q20 (3)

- q21 (3)

- q22 (3)

- q23 (3)

- q24 (3)

- q25 (3)

- q26 (3)

- q27 (3)

- q28 (3)

- q29 (4)

- q30 (4)

- q31 (4)

- q32 (4)

