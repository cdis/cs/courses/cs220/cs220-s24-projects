# Lab P7: Dictionaries

In this lab, you will analyze water accessibility data using dictionaries.

### Corrections/Clarifications

None yet

**Find any issues?** Report to us:

- Yuheng Wu <yuheng.wu@wisc.edu>

------------------------------

## Learning Objectives

In this lab, you will practice how to...
* access and utilize data in CSV files,
* deal with real-world datasets,
* use dictionaries to organize data into key, value pairs.

------------------------------
## Segment 1: Setup

Create a `lab-p7` directory and download the following files into the `lab-p7` directory.

* `small_soccer_stars.csv`
* `lab-p7.ipynb`
* `public_tests.py`

If you found your `.csv` file is downloaded as a `.txt` file (e.g. `small_soccer_stars.txt` instead of `small_soccer_stars.csv`), run `mv small_soccer_stars.txt small_soccer_stars.csv` from your Powershell/Terminal to change the extension of the file into `.csv` file manually. All the data that we need for P7 is stored in `small_soccer_stars.csv`

To start, familiarize yourself with the dataset (`small_soccer_stars.csv`). Examine its contents using Microsoft Excel, Numbers (Mac) or any other spreadsheet viewing software.

------------------------------

## Segment 2: Using dictionaries to analyze the data

You will be finishing the rest of your lab on `lab-p7.ipynb`. Run the command `jupyter notebook` from your Terminal/PowerShell window.
Remember not to close this
Terminal/PowerShell window while Jupyter is running, and open a new Terminal/PowerShell
window if necessary.

**Note**: For P7, you will be working on `p7.ipynb`, which is very similar to `lab-p7.ipynb`. We
strongly recommend that you finish working on this notebook during the lab, so you can ask
your TA/PM any questions about the notebook that may arise.

------------------------------

## Project 7

You can now get started with [P7]((https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/p7)). **You may use any helper functions created here in project P7**. Remember to only work with P7 with your partner from this point on. Have fun!
