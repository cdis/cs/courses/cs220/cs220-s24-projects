#!/usr/bin/python
# +
import os, json, math, copy
from collections import namedtuple
import pandas as pd
import bs4
from numpy import nan


# -

REL_TOL = 6e-04  # relative tolerance for floats
TOTAL_SCORE = 100 # total score for the project

DF_FILE = 'expected_dfs.html'
PLOT_FILE = 'expected_plots.json'

PASS = "All test cases passed!"

TEXT_FORMAT = "TEXT_FORMAT"  # question type when expected answer is a type, str, int, float, or bool
TEXT_FORMAT_UNORDERED_LIST = "TEXT_FORMAT_UNORDERED_LIST"  # question type when the expected answer is a list or a set where the order does *not* matter
TEXT_FORMAT_ORDERED_LIST = "TEXT_FORMAT_ORDERED_LIST"  # question type when the expected answer is a list or tuple where the order does matter
TEXT_FORMAT_DICT = "TEXT_FORMAT_DICT"  # question type when the expected answer is a dictionary
TEXT_FORMAT_SPECIAL_ORDERED_LIST = "TEXT_FORMAT_SPECIAL_ORDERED_LIST"  # question type when the expected answer is a list where order does matter, but with possible ties. Elements are ordered according to values in special_ordered_json (with ties allowed)
TEXT_FORMAT_NAMEDTUPLE = "TEXT_FORMAT_NAMEDTUPLE"  # question type when expected answer is a namedtuple
PNG_FORMAT_SCATTER = "PNG_FORMAT_SCATTER" # question type when the expected answer is a scatter plot
HTML_FORMAT_ORDERED = "HTML_FORMAT_ORDERED" # question type when the expected answer is a DataFrame and the order of the indices matter
HTML_FORMAT_UNORDERED = "HTML_FORMAT_UNORDERED" # question type when the expected answer is a DataFrame and the order of the indices does not matter
FILE_JSON_FORMAT = "FILE_JSON_FORMAT" # question type when the expected answer is a JSON file
SLASHES = "SLASHES" # question SUFFIX when expected answer contains paths with slashes

def get_expected_format():
    """get_expected_format() returns a dict mapping each question to the format
    of the expected answer."""
    expected_format = {'q1': 'TEXT_FORMAT',
                       'q2': 'TEXT_FORMAT',
                       'q3': 'TEXT_FORMAT',
                       'q4': 'TEXT_FORMAT',
                       'q5': 'TEXT_FORMAT',
                       'q6': 'TEXT_FORMAT',
                       'q7': 'TEXT_FORMAT',
                       'q8': 'TEXT_FORMAT',
                       'q9': 'TEXT_FORMAT',
                       'q10': 'TEXT_FORMAT',
                       'q11': 'TEXT_FORMAT',
                       'q12': 'TEXT_FORMAT',
                       'q13': 'TEXT_FORMAT',
                       'q14': 'TEXT_FORMAT',
                       'q15': 'TEXT_FORMAT',
                       'q16': 'TEXT_FORMAT',
                       'q17': 'TEXT_FORMAT_DICT',
                       'q18': 'TEXT_FORMAT_DICT',
                       'q19': 'TEXT_FORMAT',
                       'q20': 'TEXT_FORMAT',
                       'q21': 'TEXT_FORMAT_DICT',
                       'q22': 'TEXT_FORMAT',
                       'q23': 'TEXT_FORMAT',
                       'q24': 'TEXT_FORMAT_DICT',
                       'q25': 'TEXT_FORMAT_DICT',
                       'q26': 'TEXT_FORMAT',
                       'q27': 'TEXT_FORMAT',
                       'q28': 'TEXT_FORMAT_DICT',
                       'q29': 'TEXT_FORMAT_DICT',
                       'q30': 'TEXT_FORMAT_DICT',
                       'q31': 'TEXT_FORMAT_DICT',
                       'q32': 'TEXT_FORMAT_DICT',
                       'q33': 'TEXT_FORMAT_DICT',
                       'q34': 'TEXT_FORMAT_DICT',
                       'q35': 'TEXT_FORMAT_DICT',
                       'q36': 'TEXT_FORMAT_DICT',
                       'q37': 'TEXT_FORMAT_DICT',
                       'q38': 'TEXT_FORMAT',
                       'q39': 'TEXT_FORMAT',
                       'q40': 'TEXT_FORMAT'}
    return expected_format


def get_expected_json():
    """get_expected_json() returns a dict mapping each question to the expected
    answer (if the format permits it)."""
    expected_json = {'q1': 'E. Haaland',
                     'q2': 'Paris Saint Germain',
                     'q3': 32,
                     'q4': '188cm',
                     'q5': 188,
                     'q6': '€250K',
                     'q7': '€158.5M',
                     'q8': 250000,
                     'q9': 158500000,
                     'q10': 80000000,
                     'q11': 140000,
                     'q12': 86,
                     'q13': 79.8,
                     'q14': 193,
                     'q15': 'Right',
                     'q16': 'Inter',
                     'q17': {'ID': 239085,
                             'Name': 'E. Haaland',
                             'Age': 22,
                             'Nationality': 'Norway',
                             'Team': 'Manchester City',
                             'League': 'Premier League (England)',
                             'Value': 185000000,
                             'Wage': 340000,
                             'Attacking': 78.6,
                             'Movement': 83.6,
                             'Defending': 38.0,
                             'Goalkeeping': 10.4,
                             'Overall rating': 91,
                             'Position': 'ST',
                             'Height': 195,
                             'Preferred foot': 'Left'},
                     'q18': {'ID': 231747,
                             'Name': 'K. Mbappé',
                             'Age': 24,
                             'Nationality': 'France',
                             'Team': 'Paris Saint Germain',
                             'League': 'Ligue 1 (France)',
                             'Value': 181500000,
                             'Wage': 230000,
                             'Attacking': 83.0,
                             'Movement': 92.4,
                             'Defending': 30.7,
                             'Goalkeeping': 8.4,
                             'Overall rating': 91,
                             'Position': 'ST',
                             'Height': 182,
                             'Preferred foot': 'Right'},
                     'q19': 'ST',
                     'q20': 182,
                     'q21': {239085: 83.6,
                             231747: 92.4,
                             192985: 78.8,
                             202126: 74.0,
                             192119: 58.0,
                             188545: 80.8,
                             158023: 87.0,
                             264012: 88.8,
                             239818: 65.6,
                             238794: 90.8,
                             231866: 68.4,
                             212831: 54.6,
                             209331: 90.2,
                             203376: 70.0,
                             192448: 52.4,
                             190871: 87.4,
                             165153: 78.4,
                             252371: 82.2,
                             239053: 81.8,
                             231478: 85.4,
                             218667: 84.4,
                             210257: 64.6,
                             200389: 58.4,
                             194765: 84.8,
                             121939: 80.2,
                             256630: 84.0,
                             235073: 48.2,
                             232293: 82.8,
                             230621: 57.6,
                             228702: 83.4,
                             222665: 79.2,
                             215698: 57.0,
                             212622: 79.4,
                             212198: 76.8,
                             207865: 75.8,
                             200145: 64.8,
                             200104: 83.6,
                             193080: 56.4,
                             177003: 82.8,
                             167495: 53.6,
                             153079: 80.8,
                             265674: 86.6,
                             256790: 88.0,
                             253163: 71.4,
                             251854: 84.4,
                             247635: 83.4,
                             246669: 85.4,
                             241721: 87.4,
                             241096: 82.8,
                             240130: 77.4},
                     'q22': 65.6,
                     'q23': 90.2,
                     'q24': {239085: {'ID': 239085,
                                      'Name': 'E. Haaland',
                                      'Age': 22,
                                      'Nationality': 'Norway',
                                      'Team': 'Manchester City',
                                      'League': 'Premier League (England)',
                                      'Value': 185000000,
                                      'Wage': 340000,
                                      'Attacking': 78.6,
                                      'Movement': 83.6,
                                      'Defending': 38.0,
                                      'Goalkeeping': 10.4,
                                      'Overall rating': 91,
                                      'Position': 'ST',
                                      'Height': 195,
                                      'Preferred foot': 'Left'},
                             231747: {'ID': 231747,
                                      'Name': 'K. Mbappé',
                                      'Age': 24,
                                      'Nationality': 'France',
                                      'Team': 'Paris Saint Germain',
                                      'League': 'Ligue 1 (France)',
                                      'Value': 181500000,
                                      'Wage': 230000,
                                      'Attacking': 83.0,
                                      'Movement': 92.4,
                                      'Defending': 30.7,
                                      'Goalkeeping': 8.4,
                                      'Overall rating': 91,
                                      'Position': 'ST',
                                      'Height': 182,
                                      'Preferred foot': 'Right'},
                             192985: {'ID': 192985,
                                      'Name': 'K. De Bruyne',
                                      'Age': 32,
                                      'Nationality': 'Belgium',
                                      'Team': 'Manchester City',
                                      'League': 'Premier League (England)',
                                      'Value': 103000000,
                                      'Wage': 350000,
                                      'Attacking': 82.4,
                                      'Movement': 78.8,
                                      'Defending': 63.0,
                                      'Goalkeeping': 11.2,
                                      'Overall rating': 91,
                                      'Position': 'CM',
                                      'Height': 181,
                                      'Preferred foot': 'Right'},
                             202126: {'ID': 202126,
                                      'Name': 'H. Kane',
                                      'Age': 29,
                                      'Nationality': 'England',
                                      'Team': 'FC Bayern München',
                                      'League': 'Bundesliga (Germany)',
                                      'Value': 119500000,
                                      'Wage': 170000,
                                      'Attacking': 88.0,
                                      'Movement': 74.0,
                                      'Defending': 43.3,
                                      'Goalkeeping': 10.8,
                                      'Overall rating': 90,
                                      'Position': 'ST',
                                      'Height': 188,
                                      'Preferred foot': 'Right'},
                             192119: {'ID': 192119,
                                      'Name': 'T. Courtois',
                                      'Age': 31,
                                      'Nationality': 'Belgium',
                                      'Team': 'Real Madrid',
                                      'League': 'La Liga (Spain)',
                                      'Value': 63000000,
                                      'Wage': 250000,
                                      'Attacking': 17.2,
                                      'Movement': 58.0,
                                      'Defending': 18.0,
                                      'Goalkeeping': 86.6,
                                      'Overall rating': 90,
                                      'Position': 'GK',
                                      'Height': 200,
                                      'Preferred foot': 'Left'},
                             188545: {'ID': 188545,
                                      'Name': 'R. Lewandowski',
                                      'Age': 34,
                                      'Nationality': 'Poland',
                                      'Team': 'FC Barcelona',
                                      'League': 'La Liga (Spain)',
                                      'Value': 58000000,
                                      'Wage': 340000,
                                      'Attacking': 86.6,
                                      'Movement': 80.8,
                                      'Defending': 32.0,
                                      'Goalkeeping': 10.2,
                                      'Overall rating': 90,
                                      'Position': 'ST',
                                      'Height': 185,
                                      'Preferred foot': 'Right'},
                             158023: {'ID': 158023,
                                      'Name': 'L. Messi',
                                      'Age': 36,
                                      'Nationality': 'Argentina',
                                      'Team': 'Inter Miami',
                                      'League': 'Major League Soccer (United States)',
                                      'Value': 41000000,
                                      'Wage': 23000,
                                      'Attacking': 81.8,
                                      'Movement': 87.0,
                                      'Defending': 26.3,
                                      'Goalkeeping': 10.8,
                                      'Overall rating': 90,
                                      'Position': 'CAM',
                                      'Height': 169,
                                      'Preferred foot': 'Left'},
                             264012: {'ID': 264012,
                                      'Name': 'S. Smith',
                                      'Age': 22,
                                      'Nationality': 'United States',
                                      'Team': 'Portland Thorns W',
                                      'League': "National Women's Soccer League (United States)",
                                      'Value': 138000000,
                                      'Wage': 2000,
                                      'Attacking': 82.6,
                                      'Movement': 88.8,
                                      'Defending': 40.7,
                                      'Goalkeeping': 11.4,
                                      'Overall rating': 89,
                                      'Position': 'ST',
                                      'Height': 164,
                                      'Preferred foot': 'Right'},
                             239818: {'ID': 239818,
                                      'Name': 'Rúben Dias',
                                      'Age': 26,
                                      'Nationality': 'Portugal',
                                      'Team': 'Manchester City',
                                      'League': 'Premier League (England)',
                                      'Value': 106500000,
                                      'Wage': 250000,
                                      'Attacking': 57.0,
                                      'Movement': 65.6,
                                      'Defending': 89.7,
                                      'Goalkeeping': 9.4,
                                      'Overall rating': 89,
                                      'Position': 'CB',
                                      'Height': 187,
                                      'Preferred foot': 'Right'},
                             238794: {'ID': 238794,
                                      'Name': 'Vini Jr.',
                                      'Age': 22,
                                      'Nationality': 'Brazil',
                                      'Team': 'Real Madrid',
                                      'League': 'La Liga (Spain)',
                                      'Value': 158500000,
                                      'Wage': 310000,
                                      'Attacking': 73.8,
                                      'Movement': 90.8,
                                      'Defending': 25.0,
                                      'Goalkeeping': 7.2,
                                      'Overall rating': 89,
                                      'Position': 'LW',
                                      'Height': 176,
                                      'Preferred foot': 'Right'},
                             231866: {'ID': 231866,
                                      'Name': 'Rodri',
                                      'Age': 27,
                                      'Nationality': 'Spain',
                                      'Team': 'Manchester City',
                                      'League': 'Premier League (England)',
                                      'Value': 112000000,
                                      'Wage': 250000,
                                      'Attacking': 74.0,
                                      'Movement': 68.4,
                                      'Defending': 85.0,
                                      'Goalkeeping': 9.8,
                                      'Overall rating': 89,
                                      'Position': 'CDM',
                                      'Height': 191,
                                      'Preferred foot': 'Right'},
                             212831: {'ID': 212831,
                                      'Name': 'Alisson',
                                      'Age': 30,
                                      'Nationality': 'Brazil',
                                      'Team': 'Liverpool',
                                      'League': 'Premier League (England)',
                                      'Value': 68500000,
                                      'Wage': 180000,
                                      'Attacking': 27.8,
                                      'Movement': 54.6,
                                      'Defending': 16.7,
                                      'Goalkeeping': 87.0,
                                      'Overall rating': 89,
                                      'Position': 'GK',
                                      'Height': 193,
                                      'Preferred foot': 'Right'},
                             209331: {'ID': 209331,
                                      'Name': 'M. Salah',
                                      'Age': 31,
                                      'Nationality': 'Egypt',
                                      'Team': 'Liverpool',
                                      'League': 'Premier League (England)',
                                      'Value': 85500000,
                                      'Wage': 260000,
                                      'Attacking': 79.8,
                                      'Movement': 90.2,
                                      'Defending': 40.7,
                                      'Goalkeeping': 12.4,
                                      'Overall rating': 89,
                                      'Position': 'RW',
                                      'Height': 175,
                                      'Preferred foot': 'Left'},
                             203376: {'ID': 203376,
                                      'Name': 'V. van Dijk',
                                      'Age': 31,
                                      'Nationality': 'Netherlands',
                                      'Team': 'Liverpool',
                                      'League': 'Premier League (England)',
                                      'Value': 70500000,
                                      'Wage': 220000,
                                      'Attacking': 63.0,
                                      'Movement': 70.0,
                                      'Defending': 89.0,
                                      'Goalkeeping': 11.6,
                                      'Overall rating': 89,
                                      'Position': 'CB',
                                      'Height': 193,
                                      'Preferred foot': 'Right'},
                             192448: {'ID': 192448,
                                      'Name': 'M. ter Stegen',
                                      'Age': 31,
                                      'Nationality': 'Germany',
                                      'Team': 'FC Barcelona',
                                      'League': 'La Liga (Spain)',
                                      'Value': 54500000,
                                      'Wage': 220000,
                                      'Attacking': 23.6,
                                      'Movement': 52.4,
                                      'Defending': 16.0,
                                      'Goalkeeping': 87.4,
                                      'Overall rating': 89,
                                      'Position': 'GK',
                                      'Height': 187,
                                      'Preferred foot': 'Right'},
                             190871: {'ID': 190871,
                                      'Name': 'Neymar Jr',
                                      'Age': 31,
                                      'Nationality': 'Brazil',
                                      'Team': 'Al Hilal',
                                      'League': 'Pro League (Saudi Arabia)',
                                      'Value': 85500000,
                                      'Wage': 115000,
                                      'Attacking': 80.0,
                                      'Movement': 87.4,
                                      'Defending': 32.0,
                                      'Goalkeeping': 11.8,
                                      'Overall rating': 89,
                                      'Position': 'LW',
                                      'Height': 175,
                                      'Preferred foot': 'Right'},
                             165153: {'ID': 165153,
                                      'Name': 'K. Benzema',
                                      'Age': 35,
                                      'Nationality': 'France',
                                      'Team': 'Al Ittihad',
                                      'League': 'Pro League (Saudi Arabia)',
                                      'Value': 44000000,
                                      'Wage': 89000,
                                      'Attacking': 86.6,
                                      'Movement': 78.4,
                                      'Defending': 28.3,
                                      'Goalkeeping': 8.2,
                                      'Overall rating': 89,
                                      'Position': 'CF',
                                      'Height': 185,
                                      'Preferred foot': 'Right'},
                             252371: {'ID': 252371,
                                      'Name': 'J. Bellingham',
                                      'Age': 20,
                                      'Nationality': 'England',
                                      'Team': 'Real Madrid',
                                      'League': 'La Liga (Spain)',
                                      'Value': 128500000,
                                      'Wage': 230000,
                                      'Attacking': 77.0,
                                      'Movement': 82.2,
                                      'Defending': 77.7,
                                      'Goalkeeping': 9.6,
                                      'Overall rating': 88,
                                      'Position': 'CAM',
                                      'Height': 186,
                                      'Preferred foot': 'Right'},
                             239053: {'ID': 239053,
                                      'Name': 'F. Valverde',
                                      'Age': 24,
                                      'Nationality': 'Uruguay',
                                      'Team': 'Real Madrid',
                                      'League': 'La Liga (Spain)',
                                      'Value': 130500000,
                                      'Wage': 270000,
                                      'Attacking': 74.4,
                                      'Movement': 81.8,
                                      'Defending': 81.0,
                                      'Goalkeeping': 9.0,
                                      'Overall rating': 88,
                                      'Position': 'CM',
                                      'Height': 182,
                                      'Preferred foot': 'Right'},
                             231478: {'ID': 231478,
                                      'Name': 'L. Martínez',
                                      'Age': 25,
                                      'Nationality': 'Argentina',
                                      'Team': 'Inter',
                                      'League': 'Serie A (Italy)',
                                      'Value': 116000000,
                                      'Wage': 180000,
                                      'Attacking': 80.6,
                                      'Movement': 85.4,
                                      'Defending': 43.3,
                                      'Goalkeeping': 9.6,
                                      'Overall rating': 88,
                                      'Position': 'ST',
                                      'Height': 174,
                                      'Preferred foot': 'Right'},
                             218667: {'ID': 218667,
                                      'Name': 'Bernardo Silva',
                                      'Age': 28,
                                      'Nationality': 'Portugal',
                                      'Team': 'Manchester City',
                                      'League': 'Premier League (England)',
                                      'Value': 91500000,
                                      'Wage': 270000,
                                      'Attacking': 77.0,
                                      'Movement': 84.4,
                                      'Defending': 68.7,
                                      'Goalkeeping': 10.8,
                                      'Overall rating': 88,
                                      'Position': 'CM',
                                      'Height': 173,
                                      'Preferred foot': 'Left'},
                             210257: {'ID': 210257,
                                      'Name': 'Ederson',
                                      'Age': 29,
                                      'Nationality': 'Brazil',
                                      'Team': 'Manchester City',
                                      'League': 'Premier League (England)',
                                      'Value': 70000000,
                                      'Wage': 210000,
                                      'Attacking': 26.2,
                                      'Movement': 64.6,
                                      'Defending': 17.3,
                                      'Goalkeeping': 86.6,
                                      'Overall rating': 88,
                                      'Position': 'GK',
                                      'Height': 188,
                                      'Preferred foot': 'Left'},
                             200389: {'ID': 200389,
                                      'Name': 'J. Oblak',
                                      'Age': 30,
                                      'Nationality': 'Slovenia',
                                      'Team': 'Atlético Madrid',
                                      'League': 'La Liga (Spain)',
                                      'Value': 61000000,
                                      'Wage': 90000,
                                      'Attacking': 19.0,
                                      'Movement': 58.4,
                                      'Defending': 19.7,
                                      'Goalkeeping': 85.2,
                                      'Overall rating': 88,
                                      'Position': 'GK',
                                      'Height': 188,
                                      'Preferred foot': 'Right'},
                             194765: {'ID': 194765,
                                      'Name': 'A. Griezmann',
                                      'Age': 32,
                                      'Nationality': 'France',
                                      'Team': 'Atlético Madrid',
                                      'League': 'La Liga (Spain)',
                                      'Value': 74000000,
                                      'Wage': 135000,
                                      'Attacking': 86.4,
                                      'Movement': 84.8,
                                      'Defending': 57.3,
                                      'Goalkeeping': 12.6,
                                      'Overall rating': 88,
                                      'Position': 'ST',
                                      'Height': 176,
                                      'Preferred foot': 'Left'},
                             121939: {'ID': 121939,
                                      'Name': 'P. Lahm',
                                      'Age': 32,
                                      'Nationality': 'Germany',
                                      'Team': 'FC Bayern München',
                                      'League': 'Bundesliga (Germany)',
                                      'Value': 29500000,
                                      'Wage': 200000,
                                      'Attacking': 69.2,
                                      'Movement': 80.2,
                                      'Defending': 89.3,
                                      'Goalkeeping': 9.4,
                                      'Overall rating': 88,
                                      'Position': 'RWB',
                                      'Height': 170,
                                      'Preferred foot': 'Right'},
                             256630: {'ID': 256630,
                                      'Name': 'F. Wirtz',
                                      'Age': 20,
                                      'Nationality': 'Germany',
                                      'Team': 'Bayer 04 Leverkusen',
                                      'League': 'Bundesliga (Germany)',
                                      'Value': 118500000,
                                      'Wage': 77000,
                                      'Attacking': 73.2,
                                      'Movement': 84.0,
                                      'Defending': 50.3,
                                      'Goalkeeping': 11.4,
                                      'Overall rating': 87,
                                      'Position': 'CAM',
                                      'Height': 177,
                                      'Preferred foot': 'Right'},
                             235073: {'ID': 235073,
                                      'Name': 'G. Kobel',
                                      'Age': 25,
                                      'Nationality': 'Switzerland',
                                      'Team': 'Borussia Dortmund',
                                      'League': 'Bundesliga (Germany)',
                                      'Value': 81500000,
                                      'Wage': 64000,
                                      'Attacking': 17.0,
                                      'Movement': 48.2,
                                      'Defending': 15.0,
                                      'Goalkeeping': 83.6,
                                      'Overall rating': 87,
                                      'Position': 'GK',
                                      'Height': 195,
                                      'Preferred foot': 'Right'},
                             232293: {'ID': 232293,
                                      'Name': 'V. Osimhen',
                                      'Age': 24,
                                      'Nationality': 'Nigeria',
                                      'Team': 'Napoli',
                                      'League': 'Serie A (Italy)',
                                      'Value': 110000000,
                                      'Wage': 100000,
                                      'Attacking': 78.2,
                                      'Movement': 82.8,
                                      'Defending': 34.3,
                                      'Goalkeeping': 10.6,
                                      'Overall rating': 87,
                                      'Position': 'ST',
                                      'Height': 186,
                                      'Preferred foot': 'Right'},
                             230621: {'ID': 230621,
                                      'Name': 'G. Donnarumma',
                                      'Age': 24,
                                      'Nationality': 'Italy',
                                      'Team': 'Paris Saint Germain',
                                      'League': 'Ligue 1 (France)',
                                      'Value': 85000000,
                                      'Wage': 90000,
                                      'Attacking': 16.0,
                                      'Movement': 57.6,
                                      'Defending': 16.7,
                                      'Goalkeeping': 83.2,
                                      'Overall rating': 87,
                                      'Position': 'GK',
                                      'Height': 196,
                                      'Preferred foot': 'Right'},
                             228702: {'ID': 228702,
                                      'Name': 'F. de Jong',
                                      'Age': 26,
                                      'Nationality': 'Netherlands',
                                      'Team': 'FC Barcelona',
                                      'League': 'La Liga (Spain)',
                                      'Value': 103500000,
                                      'Wage': 240000,
                                      'Attacking': 76.6,
                                      'Movement': 83.4,
                                      'Defending': 76.3,
                                      'Goalkeeping': 9.8,
                                      'Overall rating': 87,
                                      'Position': 'CM',
                                      'Height': 181,
                                      'Preferred foot': 'Right'},
                             222665: {'ID': 222665,
                                      'Name': 'M. Ødegaard',
                                      'Age': 24,
                                      'Nationality': 'Norway',
                                      'Team': 'Arsenal',
                                      'League': 'Premier League (England)',
                                      'Value': 109000000,
                                      'Wage': 170000,
                                      'Attacking': 78.2,
                                      'Movement': 79.2,
                                      'Defending': 58.7,
                                      'Goalkeeping': 12.4,
                                      'Overall rating': 87,
                                      'Position': 'CAM',
                                      'Height': 178,
                                      'Preferred foot': 'Left'},
                             215698: {'ID': 215698,
                                      'Name': 'M. Maignan',
                                      'Age': 27,
                                      'Nationality': 'France',
                                      'Team': 'Milan',
                                      'League': 'Serie A (Italy)',
                                      'Value': 78000000,
                                      'Wage': 90000,
                                      'Attacking': 26.2,
                                      'Movement': 57.0,
                                      'Defending': 19.3,
                                      'Goalkeeping': 85.6,
                                      'Overall rating': 87,
                                      'Position': 'GK',
                                      'Height': 191,
                                      'Preferred foot': 'Right'},
                             212622: {'ID': 212622,
                                      'Name': 'J. Kimmich',
                                      'Age': 28,
                                      'Nationality': 'Germany',
                                      'Team': 'FC Bayern München',
                                      'League': 'Bundesliga (Germany)',
                                      'Value': 70000000,
                                      'Wage': 110000,
                                      'Attacking': 77.6,
                                      'Movement': 79.4,
                                      'Defending': 81.0,
                                      'Goalkeeping': 12.0,
                                      'Overall rating': 87,
                                      'Position': 'CDM',
                                      'Height': 177,
                                      'Preferred foot': 'Right'},
                             212198: {'ID': 212198,
                                      'Name': 'Bruno Fernandes',
                                      'Age': 28,
                                      'Nationality': 'Portugal',
                                      'Team': 'Manchester United',
                                      'League': 'Premier League (England)',
                                      'Value': 79500000,
                                      'Wage': 220000,
                                      'Attacking': 80.4,
                                      'Movement': 76.8,
                                      'Defending': 65.3,
                                      'Goalkeeping': 12.6,
                                      'Overall rating': 87,
                                      'Position': 'CM',
                                      'Height': 179,
                                      'Preferred foot': 'Right'},
                             207865: {'ID': 207865,
                                      'Name': 'Marquinhos',
                                      'Age': 29,
                                      'Nationality': 'Brazil',
                                      'Team': 'Paris Saint Germain',
                                      'League': 'Ligue 1 (France)',
                                      'Value': 73500000,
                                      'Wage': 140000,
                                      'Attacking': 66.8,
                                      'Movement': 75.8,
                                      'Defending': 89.0,
                                      'Goalkeeping': 9.4,
                                      'Overall rating': 87,
                                      'Position': 'CB',
                                      'Height': 183,
                                      'Preferred foot': 'Right'},
                             200145: {'ID': 200145,
                                      'Name': 'Casemiro',
                                      'Age': 31,
                                      'Nationality': 'Brazil',
                                      'Team': 'Manchester United',
                                      'League': 'Premier League (England)',
                                      'Value': 56000000,
                                      'Wage': 190000,
                                      'Attacking': 74.4,
                                      'Movement': 64.8,
                                      'Defending': 87.0,
                                      'Goalkeeping': 13.4,
                                      'Overall rating': 87,
                                      'Position': 'CDM',
                                      'Height': 185,
                                      'Preferred foot': 'Right'},
                             200104: {'ID': 200104,
                                      'Name': 'H. Son',
                                      'Age': 30,
                                      'Nationality': 'Korea Republic',
                                      'Team': 'Tottenham Hotspur',
                                      'League': 'Premier League (England)',
                                      'Value': 77000000,
                                      'Wage': 170000,
                                      'Attacking': 81.2,
                                      'Movement': 83.6,
                                      'Defending': 38.0,
                                      'Goalkeeping': 10.6,
                                      'Overall rating': 87,
                                      'Position': 'LW',
                                      'Height': 183,
                                      'Preferred foot': 'Right'},
                             193080: {'ID': 193080,
                                      'Name': 'De Gea',
                                      'Age': 31,
                                      'Nationality': 'Spain',
                                      'Team': 'Manchester United',
                                      'League': 'Premier League (England)',
                                      'Value': 42000000,
                                      'Wage': 150000,
                                      'Attacking': 20.6,
                                      'Movement': 56.4,
                                      'Defending': 19.0,
                                      'Goalkeeping': 82.2,
                                      'Overall rating': 87,
                                      'Position': 'GK',
                                      'Height': 192,
                                      'Preferred foot': 'Right'},
                             177003: {'ID': 177003,
                                      'Name': 'L. Modrić',
                                      'Age': 37,
                                      'Nationality': 'Croatia',
                                      'Team': 'Real Madrid',
                                      'League': 'La Liga (Spain)',
                                      'Value': 25000000,
                                      'Wage': 190000,
                                      'Attacking': 76.0,
                                      'Movement': 82.8,
                                      'Defending': 71.7,
                                      'Goalkeeping': 10.4,
                                      'Overall rating': 87,
                                      'Position': 'CM',
                                      'Height': 172,
                                      'Preferred foot': 'Right'},
                             167495: {'ID': 167495,
                                      'Name': 'M. Neuer',
                                      'Age': 37,
                                      'Nationality': 'Germany',
                                      'Team': 'FC Bayern München',
                                      'League': 'Bundesliga (Germany)',
                                      'Value': 9000000,
                                      'Wage': 55000,
                                      'Attacking': 24.8,
                                      'Movement': 53.6,
                                      'Defending': 12.7,
                                      'Goalkeeping': 86.2,
                                      'Overall rating': 87,
                                      'Position': 'GK',
                                      'Height': 193,
                                      'Preferred foot': 'Right'},
                             153079: {'ID': 153079,
                                      'Name': 'S. Agüero',
                                      'Age': 33,
                                      'Nationality': 'Argentina',
                                      'Team': 'FC Barcelona',
                                      'League': 'La Liga (Spain)',
                                      'Value': 51000000,
                                      'Wage': 260000,
                                      'Attacking': 81.2,
                                      'Movement': 80.8,
                                      'Defending': 27.7,
                                      'Goalkeeping': 11.8,
                                      'Overall rating': 87,
                                      'Position': 'ST',
                                      'Height': 173,
                                      'Preferred foot': 'Right'},
                             265674: {'ID': 265674,
                                      'Name': 'S. Bacha',
                                      'Age': 22,
                                      'Nationality': 'France',
                                      'Team': 'Olympique Lyonnais W',
                                      'League': 'Division 1 Féminine (France)',
                                      'Value': 97500000,
                                      'Wage': 1000,
                                      'Attacking': 70.2,
                                      'Movement': 86.6,
                                      'Defending': 82.3,
                                      'Goalkeeping': 11.4,
                                      'Overall rating': 86,
                                      'Position': 'LWB',
                                      'Height': 161,
                                      'Preferred foot': 'Left'},
                             256790: {'ID': 256790,
                                      'Name': 'J. Musiala',
                                      'Age': 20,
                                      'Nationality': 'Germany',
                                      'Team': 'FC Bayern München',
                                      'League': 'Bundesliga (Germany)',
                                      'Value': 134500000,
                                      'Wage': 79000,
                                      'Attacking': 69.4,
                                      'Movement': 88.0,
                                      'Defending': 65.0,
                                      'Goalkeeping': 8.4,
                                      'Overall rating': 86,
                                      'Position': 'CAM',
                                      'Height': 184,
                                      'Preferred foot': 'Right'},
                             253163: {'ID': 253163,
                                      'Name': 'R. Araujo',
                                      'Age': 24,
                                      'Nationality': 'Uruguay',
                                      'Team': 'FC Barcelona',
                                      'League': 'La Liga (Spain)',
                                      'Value': 93000000,
                                      'Wage': 175000,
                                      'Attacking': 62.6,
                                      'Movement': 71.4,
                                      'Defending': 86.0,
                                      'Goalkeeping': 10.6,
                                      'Overall rating': 86,
                                      'Position': 'CB',
                                      'Height': 188,
                                      'Preferred foot': 'Right'},
                             251854: {'ID': 251854,
                                      'Name': 'Pedri',
                                      'Age': 20,
                                      'Nationality': 'Spain',
                                      'Team': 'FC Barcelona',
                                      'League': 'La Liga (Spain)',
                                      'Value': 105000000,
                                      'Wage': 165000,
                                      'Attacking': 66.8,
                                      'Movement': 84.4,
                                      'Defending': 70.0,
                                      'Goalkeeping': 9.2,
                                      'Overall rating': 86,
                                      'Position': 'CM',
                                      'Height': 174,
                                      'Preferred foot': 'Right'},
                             247635: {'ID': 247635,
                                      'Name': 'K. Kvaratskhelia',
                                      'Age': 22,
                                      'Nationality': 'Georgia',
                                      'Team': 'Napoli',
                                      'League': 'Serie A (Italy)',
                                      'Value': 109000000,
                                      'Wage': 95000,
                                      'Attacking': 73.6,
                                      'Movement': 83.4,
                                      'Defending': 37.0,
                                      'Goalkeeping': 10.8,
                                      'Overall rating': 86,
                                      'Position': 'CAM',
                                      'Height': 183,
                                      'Preferred foot': 'Right'},
                             246669: {'ID': 246669,
                                      'Name': 'B. Saka',
                                      'Age': 21,
                                      'Nationality': 'England',
                                      'Team': 'Arsenal',
                                      'League': 'Premier League (England)',
                                      'Value': 99000000,
                                      'Wage': 150000,
                                      'Attacking': 74.6,
                                      'Movement': 85.4,
                                      'Defending': 60.7,
                                      'Goalkeeping': 10.0,
                                      'Overall rating': 86,
                                      'Position': 'RW',
                                      'Height': 178,
                                      'Preferred foot': 'Left'},
                             241721: {'ID': 241721,
                                      'Name': 'Rafael Leão',
                                      'Age': 24,
                                      'Nationality': 'Portugal',
                                      'Team': 'Milan',
                                      'League': 'Serie A (Italy)',
                                      'Value': 102500000,
                                      'Wage': 110000,
                                      'Attacking': 75.8,
                                      'Movement': 87.4,
                                      'Defending': 22.3,
                                      'Goalkeeping': 11.4,
                                      'Overall rating': 86,
                                      'Position': 'LW',
                                      'Height': 188,
                                      'Preferred foot': 'Right'},
                             241096: {'ID': 241096,
                                      'Name': 'S. Tonali',
                                      'Age': 23,
                                      'Nationality': 'Italy',
                                      'Team': 'Newcastle United',
                                      'League': 'Premier League (England)',
                                      'Value': 84000000,
                                      'Wage': 140000,
                                      'Attacking': 74.0,
                                      'Movement': 82.8,
                                      'Defending': 82.7,
                                      'Goalkeeping': 8.8,
                                      'Overall rating': 86,
                                      'Position': 'CDM',
                                      'Height': 181,
                                      'Preferred foot': 'Right'},
                             240130: {'ID': 240130,
                                      'Name': 'Éder Militão',
                                      'Age': 25,
                                      'Nationality': 'Brazil',
                                      'Team': 'Real Madrid',
                                      'League': 'La Liga (Spain)',
                                      'Value': 80000000,
                                      'Wage': 210000,
                                      'Attacking': 63.2,
                                      'Movement': 77.4,
                                      'Defending': 86.0,
                                      'Goalkeeping': 12.2,
                                      'Overall rating': 86,
                                      'Position': 'CB',
                                      'Height': 186,
                                      'Preferred foot': 'Right'}},
                     'q25': {'ID': 256630,
                             'Name': 'F. Wirtz',
                             'Age': 20,
                             'Nationality': 'Germany',
                             'Team': 'Bayer 04 Leverkusen',
                             'League': 'Bundesliga (Germany)',
                             'Value': 118500000,
                             'Wage': 77000,
                             'Attacking': 73.2,
                             'Movement': 84.0,
                             'Defending': 50.3,
                             'Goalkeeping': 11.4,
                             'Overall rating': 87,
                             'Position': 'CAM',
                             'Height': 177,
                             'Preferred foot': 'Right'},
                     'q26': 'H. Son',
                     'q27': 88,
                     'q28': {239085: 78.6,
                             231747: 83.0,
                             192985: 82.4,
                             202126: 88.0,
                             192119: 17.2,
                             188545: 86.6,
                             158023: 81.8,
                             264012: 82.6,
                             239818: 57.0,
                             238794: 73.8,
                             231866: 74.0,
                             212831: 27.8,
                             209331: 79.8,
                             203376: 63.0,
                             192448: 23.6,
                             190871: 80.0,
                             165153: 86.6,
                             252371: 77.0,
                             239053: 74.4,
                             231478: 80.6,
                             218667: 77.0,
                             210257: 26.2,
                             200389: 19.0,
                             194765: 86.4,
                             121939: 69.2,
                             256630: 73.2,
                             235073: 17.0,
                             232293: 78.2,
                             230621: 16.0,
                             228702: 76.6,
                             222665: 78.2,
                             215698: 26.2,
                             212622: 77.6,
                             212198: 80.4,
                             207865: 66.8,
                             200145: 74.4,
                             200104: 81.2,
                             193080: 20.6,
                             177003: 76.0,
                             167495: 24.8,
                             153079: 81.2,
                             265674: 70.2,
                             256790: 69.4,
                             253163: 62.6,
                             251854: 66.8,
                             247635: 73.6,
                             246669: 74.6,
                             241721: 75.8,
                             241096: 74.0,
                             240130: 63.2},
                     'q29': {'E. Haaland': 239085,
                             'K. Mbappé': 231747,
                             'K. De Bruyne': 192985,
                             'H. Kane': 202126,
                             'T. Courtois': 192119,
                             'R. Lewandowski': 188545,
                             'L. Messi': 158023,
                             'S. Smith': 264012,
                             'Rúben Dias': 239818,
                             'Vini Jr.': 238794,
                             'Rodri': 231866,
                             'Alisson': 212831,
                             'M. Salah': 209331,
                             'V. van Dijk': 203376,
                             'M. ter Stegen': 192448,
                             'Neymar Jr': 190871,
                             'K. Benzema': 165153,
                             'J. Bellingham': 252371,
                             'F. Valverde': 239053,
                             'L. Martínez': 231478,
                             'Bernardo Silva': 218667,
                             'Ederson': 210257,
                             'J. Oblak': 200389,
                             'A. Griezmann': 194765,
                             'P. Lahm': 121939,
                             'F. Wirtz': 256630,
                             'G. Kobel': 235073,
                             'V. Osimhen': 232293,
                             'G. Donnarumma': 230621,
                             'F. de Jong': 228702,
                             'M. Ødegaard': 222665,
                             'M. Maignan': 215698,
                             'J. Kimmich': 212622,
                             'Bruno Fernandes': 212198,
                             'Marquinhos': 207865,
                             'Casemiro': 200145,
                             'H. Son': 200104,
                             'De Gea': 193080,
                             'L. Modrić': 177003,
                             'M. Neuer': 167495,
                             'S. Agüero': 153079,
                             'S. Bacha': 265674,
                             'J. Musiala': 256790,
                             'R. Araujo': 253163,
                             'Pedri': 251854,
                             'K. Kvaratskhelia': 247635,
                             'B. Saka': 246669,
                             'Rafael Leão': 241721,
                             'S. Tonali': 241096,
                             'Éder Militão': 240130},
                     'q30': {'E. Haaland': 'Norway',
                             'K. Mbappé': 'France',
                             'K. De Bruyne': 'Belgium',
                             'H. Kane': 'England',
                             'T. Courtois': 'Belgium',
                             'R. Lewandowski': 'Poland',
                             'L. Messi': 'Argentina',
                             'S. Smith': 'United States',
                             'Rúben Dias': 'Portugal',
                             'Vini Jr.': 'Brazil',
                             'Rodri': 'Spain',
                             'Alisson': 'Brazil',
                             'M. Salah': 'Egypt',
                             'V. van Dijk': 'Netherlands',
                             'M. ter Stegen': 'Germany',
                             'Neymar Jr': 'Brazil',
                             'K. Benzema': 'France',
                             'J. Bellingham': 'England',
                             'F. Valverde': 'Uruguay',
                             'L. Martínez': 'Argentina',
                             'Bernardo Silva': 'Portugal',
                             'Ederson': 'Brazil',
                             'J. Oblak': 'Slovenia',
                             'A. Griezmann': 'France',
                             'P. Lahm': 'Germany',
                             'F. Wirtz': 'Germany',
                             'G. Kobel': 'Switzerland',
                             'V. Osimhen': 'Nigeria',
                             'G. Donnarumma': 'Italy',
                             'F. de Jong': 'Netherlands',
                             'M. Ødegaard': 'Norway',
                             'M. Maignan': 'France',
                             'J. Kimmich': 'Germany',
                             'Bruno Fernandes': 'Portugal',
                             'Marquinhos': 'Brazil',
                             'Casemiro': 'Brazil',
                             'H. Son': 'Korea Republic',
                             'De Gea': 'Spain',
                             'L. Modrić': 'Croatia',
                             'M. Neuer': 'Germany',
                             'S. Agüero': 'Argentina',
                             'S. Bacha': 'France',
                             'J. Musiala': 'Germany',
                             'R. Araujo': 'Uruguay',
                             'Pedri': 'Spain',
                             'K. Kvaratskhelia': 'Georgia',
                             'B. Saka': 'England',
                             'Rafael Leão': 'Portugal',
                             'S. Tonali': 'Italy',
                             'Éder Militão': 'Brazil'},
                     'q31': {'Left': 10, 'Right': 40},
                     'q32': {'Norway': 2,
                             'France': 5,
                             'Belgium': 2,
                             'England': 3,
                             'Poland': 1,
                             'Argentina': 3,
                             'United States': 1,
                             'Portugal': 4,
                             'Brazil': 7,
                             'Spain': 3,
                             'Egypt': 1,
                             'Netherlands': 2,
                             'Germany': 6,
                             'Uruguay': 2,
                             'Slovenia': 1,
                             'Switzerland': 1,
                             'Nigeria': 1,
                             'Italy': 2,
                             'Korea Republic': 1,
                             'Croatia': 1,
                             'Georgia': 1},
                     'q33': {'Norway': 156.8,
                             'France': 352.4,
                             'Belgium': 99.60000000000001,
                             'England': 239.6,
                             'Poland': 86.6,
                             'Argentina': 243.59999999999997,
                             'United States': 82.6,
                             'Portugal': 290.2,
                             'Brazil': 412.2,
                             'Spain': 161.39999999999998,
                             'Egypt': 79.8,
                             'Netherlands': 139.6,
                             'Germany': 337.79999999999995,
                             'Uruguay': 137.0,
                             'Slovenia': 19.0,
                             'Switzerland': 17.0,
                             'Nigeria': 78.2,
                             'Italy': 90.0,
                             'Korea Republic': 81.2,
                             'Croatia': 76.0,
                             'Georgia': 73.6},
                     'q34': {'E. Haaland': 83.6,
                             'K. Mbappé': 92.4,
                             'K. De Bruyne': 78.8,
                             'H. Kane': 74.0,
                             'T. Courtois': 58.0,
                             'R. Lewandowski': 80.8,
                             'L. Messi': 87.0,
                             'S. Smith': 88.8,
                             'Rúben Dias': 65.6,
                             'Vini Jr.': 90.8,
                             'Rodri': 68.4,
                             'Alisson': 54.6,
                             'M. Salah': 90.2,
                             'V. van Dijk': 70.0,
                             'M. ter Stegen': 52.4,
                             'Neymar Jr': 87.4,
                             'K. Benzema': 78.4,
                             'J. Bellingham': 82.2,
                             'F. Valverde': 81.8,
                             'L. Martínez': 85.4,
                             'Bernardo Silva': 84.4,
                             'Ederson': 64.6,
                             'J. Oblak': 58.4,
                             'A. Griezmann': 84.8,
                             'P. Lahm': 80.2,
                             'F. Wirtz': 84.0,
                             'G. Kobel': 48.2,
                             'V. Osimhen': 82.8,
                             'G. Donnarumma': 57.6,
                             'F. de Jong': 83.4,
                             'M. Ødegaard': 79.2,
                             'M. Maignan': 57.0,
                             'J. Kimmich': 79.4,
                             'Bruno Fernandes': 76.8,
                             'Marquinhos': 75.8,
                             'Casemiro': 64.8,
                             'H. Son': 83.6,
                             'De Gea': 56.4,
                             'L. Modrić': 82.8,
                             'M. Neuer': 53.6,
                             'S. Agüero': 80.8,
                             'S. Bacha': 86.6,
                             'J. Musiala': 88.0,
                             'R. Araujo': 71.4,
                             'Pedri': 84.4,
                             'K. Kvaratskhelia': 83.4,
                             'B. Saka': 85.4,
                             'Rafael Leão': 87.4,
                             'S. Tonali': 82.8,
                             'Éder Militão': 77.4},
                     'q35': {'E. Haaland': 78.6,
                             'K. Mbappé': 83.0,
                             'K. De Bruyne': 82.4,
                             'H. Kane': 88.0,
                             'T. Courtois': 17.2,
                             'R. Lewandowski': 86.6,
                             'L. Messi': 81.8,
                             'S. Smith': 82.6,
                             'Rúben Dias': 57.0,
                             'Vini Jr.': 73.8,
                             'Rodri': 74.0,
                             'Alisson': 27.8,
                             'M. Salah': 79.8,
                             'V. van Dijk': 63.0,
                             'M. ter Stegen': 23.6,
                             'Neymar Jr': 80.0,
                             'K. Benzema': 86.6,
                             'J. Bellingham': 77.0,
                             'F. Valverde': 74.4,
                             'L. Martínez': 80.6,
                             'Bernardo Silva': 77.0,
                             'Ederson': 26.2,
                             'J. Oblak': 19.0,
                             'A. Griezmann': 86.4,
                             'P. Lahm': 69.2,
                             'F. Wirtz': 73.2,
                             'G. Kobel': 17.0,
                             'V. Osimhen': 78.2,
                             'G. Donnarumma': 16.0,
                             'F. de Jong': 76.6,
                             'M. Ødegaard': 78.2,
                             'M. Maignan': 26.2,
                             'J. Kimmich': 77.6,
                             'Bruno Fernandes': 80.4,
                             'Marquinhos': 66.8,
                             'Casemiro': 74.4,
                             'H. Son': 81.2,
                             'De Gea': 20.6,
                             'L. Modrić': 76.0,
                             'M. Neuer': 24.8,
                             'S. Agüero': 81.2,
                             'S. Bacha': 70.2,
                             'J. Musiala': 69.4,
                             'R. Araujo': 62.6,
                             'Pedri': 66.8,
                             'K. Kvaratskhelia': 73.6,
                             'B. Saka': 74.6,
                             'Rafael Leão': 75.8,
                             'S. Tonali': 74.0,
                             'Éder Militão': 63.2},
                     'q36': {'E. Haaland': 162.2,
                             'K. Mbappé': 175.4,
                             'K. De Bruyne': 161.2,
                             'H. Kane': 162.0,
                             'T. Courtois': 75.2,
                             'R. Lewandowski': 167.39999999999998,
                             'L. Messi': 168.8,
                             'S. Smith': 171.39999999999998,
                             'Rúben Dias': 122.6,
                             'Vini Jr.': 164.6,
                             'Rodri': 142.4,
                             'Alisson': 82.4,
                             'M. Salah': 170.0,
                             'V. van Dijk': 133.0,
                             'M. ter Stegen': 76.0,
                             'Neymar Jr': 167.4,
                             'K. Benzema': 165.0,
                             'J. Bellingham': 159.2,
                             'F. Valverde': 156.2,
                             'L. Martínez': 166.0,
                             'Bernardo Silva': 161.4,
                             'Ederson': 90.8,
                             'J. Oblak': 77.4,
                             'A. Griezmann': 171.2,
                             'P. Lahm': 149.4,
                             'F. Wirtz': 157.2,
                             'G. Kobel': 65.2,
                             'V. Osimhen': 161.0,
                             'G. Donnarumma': 73.6,
                             'F. de Jong': 160.0,
                             'M. Ødegaard': 157.4,
                             'M. Maignan': 83.2,
                             'J. Kimmich': 157.0,
                             'Bruno Fernandes': 157.2,
                             'Marquinhos': 142.6,
                             'Casemiro': 139.2,
                             'H. Son': 164.8,
                             'De Gea': 77.0,
                             'L. Modrić': 158.8,
                             'M. Neuer': 78.4,
                             'S. Agüero': 162.0,
                             'S. Bacha': 156.8,
                             'J. Musiala': 157.4,
                             'R. Araujo': 134.0,
                             'Pedri': 151.2,
                             'K. Kvaratskhelia': 157.0,
                             'B. Saka': 160.0,
                             'Rafael Leão': 163.2,
                             'S. Tonali': 156.8,
                             'Éder Militão': 140.60000000000002},
                     'q37': {'Norway': 78.4,
                             'France': 70.47999999999999,
                             'Belgium': 49.800000000000004,
                             'England': 79.86666666666666,
                             'Poland': 86.6,
                             'Argentina': 81.19999999999999,
                             'United States': 82.6,
                             'Portugal': 72.55,
                             'Brazil': 58.885714285714286,
                             'Spain': 53.79999999999999,
                             'Egypt': 79.8,
                             'Netherlands': 69.8,
                             'Germany': 56.29999999999999,
                             'Uruguay': 68.5,
                             'Slovenia': 19.0,
                             'Switzerland': 17.0,
                             'Nigeria': 78.2,
                             'Italy': 45.0,
                             'Korea Republic': 81.2,
                             'Croatia': 76.0,
                             'Georgia': 73.6},
                     'q38': 'Poland',
                     'q39': 'H. Kane',
                     'q40': 'G. Kobel'}
    return expected_json


def get_special_json():
    """get_special_json() returns a dict mapping each question to the expected
    answer stored in a special format as a list of tuples. Each tuple contains
    the element expected in the list, and its corresponding value. Any two
    elements with the same value can appear in any order in the actual list,
    but if two elements have different values, then they must appear in the
    same order as in the expected list of tuples."""
    special_json = {}
    return special_json


def compare(expected, actual, q_format=TEXT_FORMAT):
    """compare(expected, actual) is used to compare when the format of
    the expected answer is known for certain."""
    try:
        if SLASHES in q_format:
            q_format = q_format.replace(SLASHES, "").strip("_ ")
            expected = clean_slashes(expected)
            actual = clean_slashes(actual)
            
        if q_format == TEXT_FORMAT:
            return simple_compare(expected, actual)
        elif q_format == TEXT_FORMAT_UNORDERED_LIST:
            return list_compare_unordered(expected, actual)
        elif q_format == TEXT_FORMAT_ORDERED_LIST:
            return list_compare_ordered(expected, actual)
        elif q_format == TEXT_FORMAT_DICT:
            return dict_compare(expected, actual)
        elif q_format == TEXT_FORMAT_SPECIAL_ORDERED_LIST:
            return list_compare_special(expected, actual)
        elif q_format == TEXT_FORMAT_NAMEDTUPLE:
            return namedtuple_compare(expected, actual)
        elif q_format == PNG_FORMAT_SCATTER:
            return compare_flip_dicts(expected, actual)
        elif q_format == HTML_FORMAT_ORDERED:
            return table_compare_ordered(expected, actual)
        elif q_format == HTML_FORMAT_UNORDERED:
            return table_compare_unordered(expected, actual)
        elif q_format == FILE_JSON_FORMAT:
            return compare_file_json(expected, actual)
        else:
            if expected != actual:
                return "expected %s but found %s " % (repr(expected), repr(actual))
    except:
        if expected != actual:
            return "expected %s but found %s " % (repr(expected), repr(actual))
    return PASS


def print_message(expected, actual, complete_msg=True):
    """print_message(expected, actual) displays a simple error message."""
    msg = "expected %s" % (repr(expected))
    if complete_msg:
        msg = msg + " but found %s" % (repr(actual))
    return msg


def simple_compare(expected, actual, complete_msg=True):
    """simple_compare(expected, actual) is used to compare when the expected answer
    is a type/Nones/str/int/float/bool. When the expected answer is a float,
    the actual answer is allowed to be within the tolerance limit. Otherwise,
    the values must match exactly, or a very simple error message is displayed."""
    msg = PASS
    if 'numpy' in repr(type((actual))):
        actual = actual.item()
    if isinstance(expected, type):
        if expected != actual:
            if isinstance(actual, type):
                msg = "expected %s but found %s" % (expected.__name__, actual.__name__)
            else:
                msg = "expected %s but found %s" % (expected.__name__, repr(actual))
            return msg
    elif not isinstance(actual, type(expected)):
        if not (isinstance(expected, (float, int)) and isinstance(actual, (float, int))) and not is_namedtuple(expected):
            return "expected to find type %s but found type %s" % (type(expected).__name__, type(actual).__name__)
    if isinstance(expected, float):
        if not math.isclose(actual, expected, rel_tol=REL_TOL):
            msg = print_message(expected, actual, complete_msg)
    elif isinstance(expected, (list, tuple)) or is_namedtuple(expected):
        new_msg = print_message(expected, actual, complete_msg)
        if len(expected) != len(actual):
            return new_msg
        for i in range(len(expected)):
            val = simple_compare(expected[i], actual[i])
            if val != PASS:
                return new_msg
    elif isinstance(expected, dict):
        new_msg = print_message(expected, actual, complete_msg)
        if len(expected) != len(actual):
            return new_msg
        val = simple_compare(sorted(list(expected.keys())), sorted(list(actual.keys())))
        if val != PASS:
            return new_msg
        for key in expected:
            val = simple_compare(expected[key], actual[key])
            if val != PASS:
                return new_msg
    else:
        if expected != actual:
            msg = print_message(expected, actual, complete_msg)
    return msg


def intelligent_compare(expected, actual, obj=None):
    """intelligent_compare(expected, actual) is used to compare when the
    data type of the expected answer is not known for certain, and default
    assumptions  need to be made."""
    if obj == None:
        obj = type(expected).__name__
    if is_namedtuple(expected):
        msg = namedtuple_compare(expected, actual)
    elif isinstance(expected, (list, tuple)):
        msg = list_compare_ordered(expected, actual, obj)
    elif isinstance(expected, set):
        msg = list_compare_unordered(expected, actual, obj)
    elif isinstance(expected, (dict)):
        msg = dict_compare(expected, actual)
    else:
        msg = simple_compare(expected, actual)
    msg = msg.replace("CompDict", "dict").replace("CompSet", "set").replace("NewNone", "None")
    return msg


def is_namedtuple(obj, init_check=True):
    """is_namedtuple(obj) returns True if `obj` is a namedtuple object
    defined in the test file."""
    bases = type(obj).__bases__
    if len(bases) != 1 or bases[0] != tuple:
        return False
    fields = getattr(type(obj), '_fields', None)
    if not isinstance(fields, tuple):
        return False
    if init_check and not type(obj).__name__ in [nt.__name__ for nt in _expected_namedtuples]:
        return False
    return True


def list_compare_ordered(expected, actual, obj=None):
    """list_compare_ordered(expected, actual) is used to compare when the
    expected answer is a list/tuple, where the order of the elements matters."""
    msg = PASS
    if not isinstance(actual, type(expected)):
        msg = "expected to find type %s but found type %s" % (type(expected).__name__, type(actual).__name__)
        return msg
    if obj == None:
        obj = type(expected).__name__
    for i in range(len(expected)):
        if i >= len(actual):
            msg = "at index %d of the %s, expected missing %s" % (i, obj, repr(expected[i]))
            break
        val = intelligent_compare(expected[i], actual[i], "sub" + obj)
        if val != PASS:
            msg = "at index %d of the %s, " % (i, obj) + val
            break
    if len(actual) > len(expected) and msg == PASS:
        msg = "at index %d of the %s, found unexpected %s" % (len(expected), obj, repr(actual[len(expected)]))
    if len(expected) != len(actual):
        msg = msg + " (found %d entries in %s, but expected %d)" % (len(actual), obj, len(expected))

    if len(expected) > 0:
        try:
            if msg != PASS and list_compare_unordered(expected, actual, obj) == PASS:
                msg = msg + " (%s may not be ordered as required)" % (obj)
        except:
            pass
    return msg


def list_compare_helper(larger, smaller):
    """list_compare_helper(larger, smaller) is a helper function which takes in
    two lists of possibly unequal sizes and finds the item that is not present
    in the smaller list, if there is such an element."""
    msg = PASS
    j = 0
    for i in range(len(larger)):
        if i == len(smaller):
            msg = "expected %s" % (repr(larger[i]))
            break
        found = False
        while not found:
            if j == len(smaller):
                val = simple_compare(larger[i], smaller[j - 1], complete_msg=False)
                break
            val = simple_compare(larger[i], smaller[j], complete_msg=False)
            j += 1
            if val == PASS:
                found = True
                break
        if not found:
            msg = val
            break
    return msg

class NewNone():
    """alternate class in place of None, which allows for comparison with
    all other data types."""
    def __str__(self):
        return 'None'
    def __repr__(self):
        return 'None'
    def __lt__(self, other):
        return True
    def __le__(self, other):
        return True
    def __gt__(self, other):
        return False
    def __ge__(self, other):
        return other == None
    def __eq__(self, other):
        return other == None
    def __ne__(self, other):
        return other != None

class CompDict(dict):
    """subclass of dict, which allows for comparison with other dicts."""
    def __init__(self, vals):
        super(self.__class__, self).__init__(vals)
        if type(vals) == CompDict:
            self.val = vals.val
        elif isinstance(vals, dict):
            self.val = self.get_equiv(vals)
        else:
            raise TypeError("'%s' object cannot be type casted to CompDict class" % type(vals).__name__)

    def get_equiv(self, vals):
        val = []
        for key in sorted(list(vals.keys())):
            val.append((key, vals[key]))
        return val

    def __str__(self):
        return str(dict(self.val))
    def __repr__(self):
        return repr(dict(self.val))
    def __lt__(self, other):
        return self.val < CompDict(other).val
    def __le__(self, other):
        return self.val <= CompDict(other).val
    def __gt__(self, other):
        return self.val > CompDict(other).val
    def __ge__(self, other):
        return self.val >= CompDict(other).val
    def __eq__(self, other):
        return self.val == CompDict(other).val
    def __ne__(self, other):
        return self.val != CompDict(other).val

class CompSet(set):
    """subclass of set, which allows for comparison with other sets."""
    def __init__(self, vals):
        super(self.__class__, self).__init__(vals)
        if type(vals) == CompSet:
            self.val = vals.val
        elif isinstance(vals, set):
            self.val = self.get_equiv(vals)
        else:
            raise TypeError("'%s' object cannot be type casted to CompSet class" % type(vals).__name__)

    def get_equiv(self, vals):
        return sorted(list(vals))

    def __str__(self):
        return str(set(self.val))
    def __repr__(self):
        return repr(set(self.val))
    def __getitem__(self, index):
        return self.val[index]
    def __lt__(self, other):
        return self.val < CompSet(other).val
    def __le__(self, other):
        return self.val <= CompSet(other).val
    def __gt__(self, other):
        return self.val > CompSet(other).val
    def __ge__(self, other):
        return self.val >= CompSet(other).val
    def __eq__(self, other):
        return self.val == CompSet(other).val
    def __ne__(self, other):
        return self.val != CompSet(other).val

def make_sortable(item):
    """make_sortable(item) replaces all Nones in `item` with an alternate
    class that allows for comparison with str/int/float/bool/list/set/tuple/dict.
    It also replaces all dicts (and sets) with a subclass that allows for
    comparison with other dicts (and sets)."""
    if item == None:
        return NewNone()
    elif isinstance(item, (type, str, int, float, bool)):
        return item
    elif isinstance(item, (list, set, tuple)):
        new_item = []
        for subitem in item:
            new_item.append(make_sortable(subitem))
        if is_namedtuple(item):
            return type(item)(*new_item)
        elif isinstance(item, set):
            return CompSet(new_item)
        else:
            return type(item)(new_item)
    elif isinstance(item, dict):
        new_item = {}
        for key in item:
            new_item[key] = make_sortable(item[key])
        return CompDict(new_item)
    return item

def list_compare_unordered(expected, actual, obj=None):
    """list_compare_unordered(expected, actual) is used to compare when the
    expected answer is a list/set where the order of the elements does not matter."""
    msg = PASS
    if not isinstance(actual, type(expected)):
        msg = "expected to find type %s but found type %s" % (type(expected).__name__, type(actual).__name__)
        return msg
    if obj == None:
        obj = type(expected).__name__

    try:
        sort_expected = sorted(make_sortable(expected))
        sort_actual = sorted(make_sortable(actual))
    except:
        return "unexpected datatype found in %s; expected entries of type %s" % (obj, obj, type(expected[0]).__name__)

    if len(actual) == 0 and len(expected) > 0:
        msg = "in the %s, missing " % (obj) + sort_expected[0]
    elif len(actual) > 0 and len(expected) > 0:
        val = intelligent_compare(sort_expected[0], sort_actual[0])
        if val.startswith("expected to find type"):
            msg = "in the %s, " % (obj) + simple_compare(sort_expected[0], sort_actual[0])
        else:
            if len(expected) > len(actual):
                msg = "in the %s, missing " % (obj) + list_compare_helper(sort_expected, sort_actual)
            elif len(expected) < len(actual):
                msg = "in the %s, found un" % (obj) + list_compare_helper(sort_actual, sort_expected)
            if len(expected) != len(actual):
                msg = msg + " (found %d entries in %s, but expected %d)" % (len(actual), obj, len(expected))
                return msg
            else:
                val = list_compare_helper(sort_expected, sort_actual)
                if val != PASS:
                    msg = "in the %s, missing " % (obj) + val + ", but found un" + list_compare_helper(sort_actual,
                                                                                               sort_expected)
    return msg


def namedtuple_compare(expected, actual):
    """namedtuple_compare(expected, actual) is used to compare when the
    expected answer is a namedtuple defined in the test file."""
    msg = PASS
    if not is_namedtuple(actual, False):
        return "expected to find type %s namedtuple but found type %s" % (type(expected).__name__, type(actual).__name__)
    if type(expected).__name__ != type(actual).__name__:
        return "expected to find type %s namedtuple but found type %s namedtuple" % (type(expected).__name__, type(actual).__name__)
    expected_fields = expected._fields
    actual_fields = actual._fields
    msg = list_compare_ordered(list(expected_fields), list(actual_fields), "namedtuple attributes")
    if msg != PASS:
        return msg
    for field in expected_fields:
        val = intelligent_compare(getattr(expected, field), getattr(actual, field))
        if val != PASS:
            msg = "at attribute %s of namedtuple %s, " % (field, type(expected).__name__) + val
            return msg
    return msg


def clean_slashes(item):
    """clean_slashes()"""
    if isinstance(item, str):
        return type(item)(str(item).replace("\\", os.path.sep).replace("/", os.path.sep))
    elif item == None or isinstance(item, (type, int, float, bool)):
        return item
    elif isinstance(item, (list, tuple, set)) or is_namedtuple(item):
        new_item = []
        for subitem in item:
            new_item.append(clean_slashes(subitem))
        if is_namedtuple(item):
            return type(item)(*new_item)
        else:
            return type(item)(new_item)
    elif isinstance(item, dict):
        new_item = {}
        for key in item:
            new_item[clean_slashes(key)] = clean_slashes(item[key])
        return item


def list_compare_special_initialize(special_expected):
    """list_compare_special_initialize(special_expected) takes in the special
    ordering stored as a sorted list of items, and returns a list of lists
    where the ordering among the inner lists does not matter."""
    latest_val = None
    clean_special = []
    for row in special_expected:
        if latest_val == None or row[1] != latest_val:
            clean_special.append([])
            latest_val = row[1]
        clean_special[-1].append(row[0])
    return clean_special


def list_compare_special(special_expected, actual):
    """list_compare_special(special_expected, actual) is used to compare when the
    expected answer is a list with special ordering defined in `special_expected`."""
    msg = PASS
    expected_list = []
    special_order = list_compare_special_initialize(special_expected)
    for expected_item in special_order:
        expected_list.extend(expected_item)
    val = list_compare_unordered(expected_list, actual)
    if val != PASS:
        return val
    i = 0
    for expected_item in special_order:
        j = len(expected_item)
        actual_item = actual[i: i + j]
        val = list_compare_unordered(expected_item, actual_item)
        if val != PASS:
            if j == 1:
                msg = "at index %d " % (i) + val
            else:
                msg = "between indices %d and %d " % (i, i + j - 1) + val
            msg = msg + " (list may not be ordered as required)"
            break
        i += j
    return msg


def dict_compare(expected, actual, obj=None):
    """dict_compare(expected, actual) is used to compare when the expected answer
    is a dict."""
    msg = PASS
    if not isinstance(actual, type(expected)):
        msg = "expected to find type %s but found type %s" % (type(expected).__name__, type(actual).__name__)
        return msg
    if obj == None:
        obj = type(expected).__name__

    expected_keys = list(expected.keys())
    actual_keys = list(actual.keys())
    val = list_compare_unordered(expected_keys, actual_keys, obj)

    if val != PASS:
        msg = "bad keys in %s: " % (obj) + val
    if msg == PASS:
        for key in expected:
            new_obj = None
            if isinstance(expected[key], (list, tuple, set)):
                new_obj = 'value'
            elif isinstance(expected[key], dict):
                new_obj = 'sub' + obj
            val = intelligent_compare(expected[key], actual[key], new_obj)
            if val != PASS:
                msg = "incorrect value for key %s in %s: " % (repr(key), obj) + val
    return msg


def is_flippable(item):
    """is_flippable(item) determines if the given dict of lists has lists of the
    same length and is therefore flippable."""
    item_lens = set(([str(len(item[key])) for key in item]))
    if len(item_lens) == 1:
        return PASS
    else:
        return "found lists of lengths %s" % (", ".join(list(item_lens)))

def flip_dict_of_lists(item):
    """flip_dict_of_lists(item) flips a dict of lists into a list of dicts if the
    lists are of same length."""
    new_item = []
    length = len(list(item.values())[0])
    for i in range(length):
        new_dict = {}
        for key in item:
            new_dict[key] = item[key][i]
        new_item.append(new_dict)
    return new_item

def compare_flip_dicts(expected, actual):
    """compare_flip_dicts(expected, actual) flips a dict of lists (or dicts) into
    a list of dicts (or dict of dicts) and then compares the list ignoring order."""
    msg = PASS
    example_item = list(expected.values())[0]
    if isinstance(example_item, (list, tuple)):
        val = is_flippable(actual)
        if val != PASS:
            msg = "expected to find lists of length %d, but " % (len(example_item)) + val
            return msg
        msg = list_compare_unordered(flip_dict_of_lists(expected), flip_dict_of_lists(actual), "lists")
    elif isinstance(example_item, dict):
        expected_keys = list(example_item.keys())
        for key in actual:
            val = list_compare_unordered(expected_keys, list(actual[key].keys()), "dictionary %s" % key)
            if val != PASS:
                return val
        for cat_key in expected_keys:
            expected_category = {}
            actual_category = {}
            for key in expected:
                expected_category[key] = expected[key][cat_key]
                actual_category[key] = actual[key][cat_key]
            val = list_compare_unordered(flip_dict_of_lists(expected_category), flip_dict_of_lists(actual_category), "category " + repr(cat_key))
            if val != PASS:
                return val
    return msg


def get_expected_tables():
    """get_expected_tables() reads the html file with the expected DataFrames
    and returns a dict mapping each question to a html table."""
    if not os.path.exists(DF_FILE):
        return None

    expected_tables = {}
    f = open(DF_FILE, encoding='utf-8')
    soup = bs4.BeautifulSoup(f.read(), 'html.parser')
    f.close()

    tables = soup.find_all('table')
    for table in tables:
        expected_tables[table.get("data-question")] = table

    return expected_tables

def parse_table(table):
    """parse_table(table) takes in a table as a html string and returns
    a dict mapping each row and column index to the value at that position."""
    rows = []
    for tr in table.find_all('tr'):
        rows.append([])
        for cell in tr.find_all(['td', 'th']):
            rows[-1].append(cell.get_text().strip("\n "))

    cells = {}
    for r in range(1, len(rows)):
        for c in range(1, len(rows[0])):
            rname = rows[r][0]
            cname = rows[0][c]
            cells[(rname,cname)] = rows[r][c]
    return cells


def get_expected_namedtuples():
    """get_expected_namedtuples() defines the required namedtuple objects
    globally. It also returns a tuple of the classes."""
    expected_namedtuples = []
    
    return tuple(expected_namedtuples)

_expected_namedtuples = get_expected_namedtuples()


def parse_df(item):
    """parse_df(item) takes in a DataFrame input in any format (i.e, a DataFrame, or as html string)
    and extracts the table in the DataFrame as a bs4.BeautifulSoup object"""
    if isinstance(item, (pd.Series)):
        item = pd.DataFrame(item)
    if isinstance(item, (pd.DataFrame)):
        item = item.to_html()
    if isinstance(item, (str)):
        item = bs4.BeautifulSoup(item, 'html.parser').find('table')
    if isinstance(item, (bs4.element.Tag)):
        return item
    return "item could not be parsed"


def parse_cells(cells):
    """parse_cells(cells) takes in DataFrame cells as input and returns a DataFrame object"""
    table = {}
    for idx in cells:
        if idx[1] not in table:
            table[idx[1]] = {}
        table[idx[1]][idx[0]] = cells[idx]
    return pd.DataFrame(table)


def compare_cell_html(expected_cells, actual_cells):
    """compare_cell_html(expected_cells, actual_cells) is used to compare all the cells
    of two DataFrames."""
    expected_cols = list(set(["column %s" % (loc[1]) for loc in expected_cells]))
    actual_cols = list(set(["column %s" % (loc[1]) for loc in actual_cells]))
    msg = list_compare_unordered(expected_cols, actual_cols, "DataFrame")
    if msg != PASS:
        return msg

    expected_rows = list(set(["row index %s" % (loc[0]) for loc in expected_cells]))
    actual_rows = list(set(["row index %s" % (loc[0]) for loc in actual_cells]))
    msg = list_compare_unordered(expected_rows, actual_rows, "DataFrame")
    if msg != PASS:
        return msg

    for location, expected in expected_cells.items():
        location_name = "column {} at index {}".format(location[1], location[0])
        actual = actual_cells.get(location, None)
        if actual == None:
            return "in %s, expected to find %s" % (location_name, repr(expected))
        try:
            actual_ans = float(actual)
            expected_ans = float(expected)
            if math.isnan(actual_ans) and math.isnan(expected_ans):
                continue
        except Exception as e:
            actual_ans, expected_ans = actual, expected
        msg = simple_compare(expected_ans, actual_ans)
        if msg != PASS:
            return "in %s, " % location_name + msg
    return PASS


def table_compare_ordered(expected, actual):
    """table_compare_ordered(expected, actual) is used to compare when the
    expected answer is a DataFrame where the order of the indices matter."""
    try:
        expected_table = parse_df(expected)
        actual_table = parse_df(actual)
    except Exception as e:
        return "expected to find type DataFrame but found type %s instead" % type(actual).__name__
    
    if not isinstance(expected_table, (bs4.element.Tag)) or not isinstance(actual_table, (bs4.element.Tag)):
        return "expected to find type DataFrames but found types %s and %s instead" % (type(expected).__name__, type(actual).__name__)
    expected_cells = parse_table(expected_table)
    actual_cells = parse_table(actual_table)
    return compare_cell_html(expected_cells, actual_cells)


def table_compare_unordered(expected, actual):
    """table_compare_unordered(expected, actual) is used to compare when the
    expected answer is a DataFrame where the order of the indices do not matter."""
    try:
        expected_table = parse_df(expected)
        actual_table = parse_df(actual)
    except Exception as e:
        return "expected to find type DataFrame but found type %s instead" % type(actual).__name__
    
    if not isinstance(expected_table, (bs4.element.Tag)) or not isinstance(actual_table, (bs4.element.Tag)):
        return "expected to find type DataFrames but found types %s and %s instead" % (type(expected).__name__, type(actual).__name__)
    expected_cells = parse_table(expected_table)
    actual_cells = parse_table(actual_table)
    
    new_expected = parse_cells(expected_cells)
    new_actual = parse_cells(actual_cells)
    
    new_expected = new_expected.sort_values(by=list(new_expected.columns)).reset_index(drop=True)
    new_actual = new_actual.sort_values(by=list(new_expected.columns)).reset_index(drop=True)[list(new_expected.columns)]
    
    return table_compare_ordered(new_expected, new_actual)


def get_expected_plots():
    """get_expected_plots() reads the json file with the expected plot data
    and returns a dict mapping each question to a dictionary with the plots data."""
    if not os.path.exists(PLOT_FILE):
        return None

    f = open(PLOT_FILE, encoding='utf-8')
    expected_plots = json.load(f)
    f.close()
    return expected_plots


def compare_file_json(expected, actual):
    """compare_file_json(expected, actual) is used to compare when the
    expected answer is a JSON file."""
    msg = PASS
    if not os.path.isfile(expected):
        return "file %s not found; make sure it is downloaded and stored in the correct directory" % (expected)
    elif not os.path.isfile(actual):
        return "file %s not found; make sure that you have created the file with the correct name" % (actual)
    try:
        e = open(expected, encoding='utf-8')
        expected_data = json.load(e)
        e.close()
    except json.JSONDecodeError:
        return "file %s is broken and cannot be parsed; please delete and redownload the file correctly" % (expected)
    try:
        a = open(actual, encoding='utf-8')
        actual_data = json.load(a)
        a.close()
    except json.JSONDecodeError:
        return "file %s is broken and cannot be parsed" % (actual)
    if isinstance(expected_data, list):
        msg = list_compare_ordered(expected_data, actual_data, 'file ' + actual)
    elif isinstance(expected_data, dict):
        msg = dict_compare(expected_data, actual_data)
    return msg


_expected_json = get_expected_json()
_special_json = get_special_json()
_expected_plots = get_expected_plots()
_expected_tables = get_expected_tables()
_expected_format = get_expected_format()

def check(qnum, actual):
    """check(qnum, actual) is used to check if the answer in the notebook is
    the correct answer, and provide useful feedback if the answer is incorrect."""
    msg = PASS
    error_msg = "<b style='color: red;'>ERROR:</b> "
    q_format = _expected_format[qnum]

    if q_format == TEXT_FORMAT_SPECIAL_ORDERED_LIST:
        expected = _special_json[qnum]
    elif q_format == PNG_FORMAT_SCATTER:
        if _expected_plots == None:
            msg = error_msg + "file %s not parsed; make sure it is downloaded and stored in the correct directory" % (PLOT_FILE)
        else:
            expected = _expected_plots[qnum]
    elif q_format in [HTML_FORMAT_ORDERED, HTML_FORMAT_UNORDERED]:
        if _expected_tables == None:
            msg = error_msg + "file %s not parsed; make sure it is downloaded and stored in the correct directory" % (DF_FILE)
        else:
            expected = _expected_tables[qnum]
    else:
        expected = _expected_json[qnum]

    if msg != PASS:
        print(msg)
    else:
        msg = compare(expected, actual, q_format)
        if msg != PASS:
            msg = error_msg + msg
        print(msg)


def rubric_check(rubric_point, check_past_errors=False):
    print('All test cases passed!')
    
def get_summary():
    print('Total Score: %d/%d\n' % (TOTAL_SCORE, TOTAL_SCORE))

def display_late_days_used():
    print()