# Lab Project 7 (Lab-P7) Grading Rubric


## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- You will receive full points if you pass the public tests associated with each question, and you will not receive any points for a question if you fail the public tests of that question.

## Rubric

### Question specific guidelines:

- q1 (2)

- q2 (2)

- q3 (2)

- q4 (2)

- q5 (2)

- q6 (2)

- q7 (2)

- q8 (2)

- q9 (2)

- q10 (2)

- q11 (2)

- q12 (2)

- q13 (2)

- q14 (2)

- q15 (2)

- q16 (2)

- q17 (2)

- q18 (2)

- q19 (2)

- q20 (2)

- q21 (3)

- q22 (3)

- q23 (3)

- q24 (3)

- q25 (3)

- q26 (3)

- q27 (3)

- q28 (3)

- q29 (3)

- q30 (3)

- q31 (3)

- q32 (3)

- q33 (3)

- q34 (3)

- q35 (3)

- q36 (3)

- q37 (3)

- q38 (3)

- q39 (3)

- q40 (3)

