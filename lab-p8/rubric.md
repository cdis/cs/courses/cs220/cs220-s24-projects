# Lab Project 8 (Lab-P8) Grading Rubric


## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- You will receive full points if you pass the public tests associated with each question, and you will not receive any points for a question if you fail the public tests of that question.

## Rubric

### Question specific guidelines:

- q1 (4)

- q2 (4)

- q3 (4)

- q4 (4)

- q5 (4)

- q6 (4)

- q7 (4)

- q8 (4)

- q9 (4)

- q10 (4)

- q11 (4)

- q12 (4)

- q13 (4)

- q14 (4)

- q15 (4)

- q16 (4)

- q17 (4)

- q18 (4)

- q19 (4)

- q20 (4)

- q21 (4)

- q22 (4)

- q23 (4)

- q24 (4)

- q25 (4)

