# Lab Project 9 (Lab-P9) Grading Rubric


## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- You will receive full points if you pass the public tests associated with each question, and you will not receive any points for a question if you fail the public tests of that question.

## Rubric

### Question specific guidelines:

- q1 (4)

- q2 (4)

- q3 (4)

- q4 (4)

- q5 (4)

- q6 (5)

- q7 (5)

- q8 (5)

- q9 (5)

- q10 (5)

- `movies_by_cast` (0)

- q11 (5)

- q12 (5)

- q13 (5)

- q14 (5)

- q15 (5)

- q16 (5)

- q17 (5)

- q18 (5)

- q19 (5)

- q20 (5)

- q21 (5)

