# Project 12 (P12): World University Rankings


## Corrections and clarifications:

* None yet.

**Find any issues?** Report to us:

- Yuheng Wu <yuheng.wu@wisc.edu>

## Instructions:

This project will focus on **Pandas**, and **BeautifulSoup**. To start, download [`p12.ipynb`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/p12/p12.ipynb), [`public_tests.py`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/blob/main/p12/public_tests.py), and [`expected_dfs.html`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/p12/expected_dfs.html).

**Important Warning:** You must **not** manually download any of the other `json` or `html` files. In particular, you are **not** allowed to manually download the files `rankings.json`, `2021.html`, `2022.html`, or `2023.html`. You **must** download these files using Python in your `p12.ipynb` notebook as a part of the project, using the `download` function. Otherwise, your code may pass on **your computer**, but **fail** on Gradescope.

You will work on `p12.ipynb` and hand it in. You should follow the provided directions for each question. Questions have **specific** directions on what **to do** and what **not to do**.

------------------------------

## IMPORTANT Submission instructions:
- Review the [Grading Rubric](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/p12/rubric.md), to ensure that you don't lose points during code review.
- Login to [Gradescope](https://www.gradescope.com/) and upload the zip file into the P12 assignment.
- If you completed the project with a **partner**, make sure to **add their name** by clicking "Add Group Member"
in Gradescope when uploading the P12 zip file.

   <img src="images/add_group_member.png" width="700">

   **Warning:** You will have to add your partner on Gradescope even if you have filled out this information in your `p12.ipynb` notebook.

- It is **your responsibility** to make sure that your project clears auto-grader tests on the Gradescope test system. Otter test results should be available within forty minutes after your submission (usually within ten minutes). **Ignore** the `-/100.00` that is displayed to the right. You should be able to see both PASS / FAIL results for the 20 test cases, which is accessible via Gradescope Dashboard (as in the image below):

    <img src="images/gradescope.png" width="700">

- You can view your **final score** at the **end of the page**. If you pass all tests, then you will receive **full points** for the project. Otherwise, you can see your final score in the **summary** section of the test results (as in the image below):

   <img src="images/summary.png" width="700">

   If you want more details on why you lost points on a particular test, you can scroll up to find more details about the test.
