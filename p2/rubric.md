# Project 2 (P2) Grading Rubric


## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- To ensure that you don't lose any points, you must **review** the rubric and make sure that you have followed the instructions provided in the project correctly.
- If you **fail** the **public tests** for a function or **hardcode** the answers to that question, you will automatically lose **all** points for that question.

## Rubric

### General guidelines:

- Did not save the notebook file prior to running the cell containing "export". We cannot see your output if you do not save before generating the zip file. This deduction will become stricter for future projects. (-1)
- Used conditionals/loops or other material not covered in class yet. (-20)

### Question specific guidelines:

- q1 (5)

- q2 (5)
	- expression is not computed using Python as required (-4)

- q3 (5)
	- expression is not computed using Python as required (-4)

- q4 (5)

- q5 (5)
	- `type` of the required expression is not found (-4)

- q6 (5)
	- `type` of the required expression is not found (-4)

- q7 (5)
	- `type` of the required expression is not found (-4)

- q8 (5)
	- `type` of the required expression is not found (-4)

- q9 (5)
	- `type` of the required expression is not found (-4)

- q10 (5)
	- `type` of the required expression is not found (-4)

- q11 (5)
	- string multiplication and concatenation are not used to compute the required string (-4)

- q12 (5)
	- answer is computed without fixing the expression by hardcoding (-4)

- q13 (5)
	- expression to compute `cube_volume` is incorrect (-3)
	- `cube_side` variable is not used in `cube_volume` computation (-1)

- q14 (5)
	- expression to compute `cylinder_volume` is incorrect (-3)
	- `cylinder_radius` variable is not defined, initialized, and used in `cylinder_volume` computation (-1)
	- `cylinder_height` variable is not defined, initialized, and used in `cylinder_volume` computation (-1)

- q15 (5)
	- expression to compute `safe_operation` is incorrect (-3)
	- `TRAILER_LIMIT` and / or `trailer_weight` values are not used to answer (-1)
	- the correct comparison operator is not used (-1)

- q16 (5)
	- expression to compute `safe_operation` is incorrect (-3)
	- `UPPER_LIMIT` and / or `LOWER_LIMIT` and / or `truck_weight` values are not used to answer (-1)
	- the correct comparison operator is not used (-1)

- q17 (5)
	- initialization value of more than one variable is changed (-3)

- q18 (5)
	- expression to compute `success` is incorrect (-3)
	- variables `short` and / or `dark` are reinitialized (-1)

- q19 (5)
	- expression to compute `primary_color` is incorrect (-3)
	- variable `color` is reinitialized (-1)

- q20 (5)
	- expression to compute `average_score` is incorrect (-3)
	- `alice_score`, `bob_score`, `chang_score`, or `divya_score` variables are not defined, initialized, and used in `average_score` computation (-2)

