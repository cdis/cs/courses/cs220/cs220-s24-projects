import csv as __csv__

# years in dataset
__years = None

# key: (agency_id, year), val: spending in millions
__data = None

# key: agency name, val: agency ID
__agency_to_id = None


def init(path):
    """init(path) must be called to load data before other calls will work.  You should call it like this: init("madison_budget.csv") or init("lab_budget.csv")"""

    global __years, __data, __agency_to_id

    if path != 'madison_budget.csv':
        print("WARNING!  Opening a path other than madison_budget.csv.  " +
              "That's fine for testing your code yourself, but madison_budget.csv " +
              "will be the only file around when we test your code " +
              "for grading.")

    __years = []
    __data = {}
    __agency_to_id = {}

    f = open(path, encoding='utf-8')
    data = list(__csv__.reader(f))
    f.close()

    for agency_idx in range(1, len(data)):
        agency = data[agency_idx][1]
        agency_id = int(data[agency_idx][0])
        __agency_to_id[agency] = agency_id
        for year_idx in range(2, len(data[0])):
            year = int(data[0][year_idx])
            if year not in __years:
                __years.append(year)
            agency_budget = float(data[agency_idx][year_idx])
            __data[(agency_id, year)] = agency_budget

def dump():
    """dump() prints all the data to the screen"""
    if __agency_to_id == None:
        raise Exception("you did not call init first")

    for agency in sorted(__agency_to_id.keys()):
        agency_id = __agency_to_id[agency]
        print("%-7s [ID %d]" % (agency, agency_id))
        for year in __years:
            print("  %d: $%f MILLION" % (year, __data[(agency_id, year)]))
        print()


def get_id(agency):
    """get_id(agency) returns the ID of the specified agency"""
    if __agency_to_id == None:
        raise Exception("you did not call init first")
    if not agency in __agency_to_id:
        raise Exception("No agency '%s', only these: %s" %
                        (str(agency), ','.join(list(__agency_to_id.keys()))))
    return __agency_to_id[agency]


def get_budget(agency_id, year=2023):
    """get_budget(agency_id, year) returns the dollars (in millions) allotted to the specified agency in specified year"""
    if __data == None:
        raise Exception("you did not call init first")
    if not (agency_id, year) in __data:
        raise Exception("No data for agency %s, in year %s" %
                        (str(agency_id), str(year)))
    return __data[(agency_id, year)]
