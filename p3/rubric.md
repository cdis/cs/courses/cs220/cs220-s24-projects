# Project 3 (P3) Grading Rubric


## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- To ensure that you don't lose any points, you must **review** the rubric and make sure that you have followed the instructions provided in the project correctly.
- If you **fail** the **public tests** for a function or **hardcode** the answers to that question, you will automatically lose **all** points for that question.

## Rubric

### General guidelines:

- Did not save the notebook file prior to running the cell containing "export". We cannot see your output if you do not save before generating the zip file. This deduction will become stricter for future projects. (-1)
- Used conditionals/loops or other material not covered in class yet. (-20)

### Question specific guidelines:

- q1 (2)

- q2 (3)
	- agency id is hardcoded (-2)
	- variable `finance_id` is not used (-1)

- q3 (3)
	- `year_max` function is not used to answer (-3)

- q4 (5)
	- `year_max` function is not used to answer (-2)
	- the built-in function `max` is not used (-2)

- `agency_min` (3)
	- function logic is incorrect (-2)
	- the built-in function `min` is not used (-1)

- q5 (3)
	- `agency_min` function is not used to answer (-3)

- q6 (5)
	- `agency_min` function is not used to answer (-2)
	- the built-in function `min` is not used (-2)

- `agency_avg` (3)
	- function logic is incorrect (-2)
	- `get_budget` and `get_id` functions are not used (-1)

- q7 (3)
	- `agency_avg` function is not used to answer (-3)

- q8 (3)
	- `agency_avg` function is not used to answer (-3)

- q9 (5)
	- average budget is computed without using the `agency_avg` function (-3)
	- `get_budget` function is not used for getting Public Health budget (-1)

- `year_budget` (4)
	- function logic is incorrect (-2)
	- agency ids are hardcoded (-1)
	- default value of `year=2024` is changed in `year_budget` (-1)

- q10 (4)
	- `year_budget` function is not used to answer (-2)
	- passed more arguments than necessary to `year_budget` function (-2)

- q11 (5)
	- `year_budget` function is not used to answer (-2)
	- passed more arguments than necessary to `year_budget` function (-2)

- `change_per_year` (5)
	- function logic is incorrect (-3)
	- default values of `start_year=2020` and `end_year=2024` are changed in `change_per_year` (-2)

- q12 (4)
	- `change_per_year` function is not used to answer (-3)
	- passed more arguments than necessary to `change_per_year` function (-1)

- q13 (4)
	- `change_per_year` function is not used to answer (-3)
	- passed more arguments than necessary to `change_per_year` function (-1)

- q14 (4)
	- `change_per_year` function is not used to answer (-3)
	- passed more arguments than necessary to `change_per_year` function (-1)

- q15 (4)
	- `change_per_year` function is not used to answer (-3)
	- passed more arguments than necessary to `change_per_year` function (-1)

- `extrapolate` (5)
	- function logic is incorrect (-2)
	- `change_per_year` function is not used (-2)
	- default values are not specified in `extrapolate` (-1)

- q16 (4)
	- `extrapolate` function is not used to answer (-3)
	- passed more arguments than necessary to `extrapolate` function (-1)

- q17 (4)
	- `extrapolate` function is not used to answer (-3)
	- passed more arguments than necessary to `extrapolate` function (-1)

- q18 (5)
	- `extrapolate` function is not used to answer (-3)
	- agency id is hardcoded (-1)
	- passed more arguments than necessary to `extrapolate` function (-1)

- q19 (5)
	- `extrapolate` function is not used to answer (-3)
	- agency id is hardcoded (-1)
	- passed more arguments than necessary to `extrapolate` function (-1)

- q20 (5)
	- agency id is hardcoded (-2)
	- `agency_avg` function is not used to compute the average budget (-1)

