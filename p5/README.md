# Project 5 (P5): Investigating Steam Game Data


## Corrections and clarifications

* In q10, we are referring to the index of the game with most number of negative reviews **in q9**, not q7.

**Find any issues?** Report to us:

- Takis Chytas <chytas@wisc.edu>
- Samuel Guo <sguo258@wisc.edu>

## Note on Academic Misconduct:
You are **allowed** to work with a partner on your projects. While it is not required that you work with a partner, it is **recommended** that you find a project partner as soon as possible as the projects will get progressively harder. Be careful **not** to work with more than one partner. If you worked with a partner on Lab-P5, you are **not** allowed to finish your project with a different partner. You may either continue to work with the same partner, or work on P5 alone. Now may be a good time to review our [course policies](https://cs220.cs.wisc.edu/s23/syllabus.html).

## Instructions:

This project will focus on **loops** and **strings**. To start, download `p5.ipynb`, `project.py`, `public_tests.py` and `steam.csv`.

**Note:** Please go through [Lab-P5](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/lab-p5) before you start the project. The lab contains some very important information that will be necessary for you to finish the project.

You will work on `p5.ipynb` and hand it in. You should follow the provided directions for each question. Questions have **specific** directions on what **to do** and what **not to do**.

After you've downloaded the file to your `p5` directory, open a terminal window and use `cd` to navigate to that directory. To make sure you're in the correct directory in the terminal, type `pwd`. To make sure you've downloaded the notebook file, type `ls` to ensure that `p5.ipynb`, `project.py`, `public_tests.py`, and `steam.csv` are listed. Then run the command `jupyter notebook` to start Jupyter, and get started on the project!

**IMPORTANT**: You should **NOT** terminate/close the session where you run the above command. If you need to use any other Terminal/PowerShell commands, open a new window instead. Keep constantly saving your notebook file, by either clicking the "Save and Checkpoint" button (floppy disk) or using the appropriate keyboard shortcut.

------------------------------

## IMPORTANT Submission instructions:
- Review the [Grading Rubric](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/p5/rubric.md), to ensure that you don't lose points during code review.

- Login to [Gradescope](https://www.gradescope.com/) and upload the zip file into the P5 assignment.

- If you completed the project with a **partner**, make sure to **add their name** by clicking "Add Group Member"
in Gradescope when uploading the P5 zip file.

   <img src="images/add_group_member.png" width="700">

   **Warning:** You will have to add your partner on Gradescope even if you have filled out this information in your `p5.ipynb` notebook.

- It is **your responsibility** to make sure that your project clears auto-grader tests on the Gradescope test system. Otter test results should be available within forty minutes after your submission (usually within ten minutes). **Ignore** the `-/100.00` that is displayed to the right. You should be able to see both PASS / FAIL results for the 20 test cases, which is accessible via Gradescope Dashboard (as in the image below):

    <img src="images/gradescope.png" width="700">

- You can view your **final score** at the **end of the page**. If you pass all tests, then you will receive **full points** for the project. Otherwise, you can see your final score in the **summary** section of the test results (as in the image below):

   <img src="images/summary.png" width="700">

   If you want more details on why you lost points on a particular test, you can scroll up to find more details about the test.
