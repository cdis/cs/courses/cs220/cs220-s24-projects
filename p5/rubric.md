# Project 5 (P5) Grading Rubric


## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- To ensure that you don't lose any points, you must **review** the rubric and make sure that you have followed the instructions provided in the project correctly.
- If you **fail** the **public tests** for a function or **hardcode** the answers to that question, you will automatically lose **all** points for that question.

## Rubric

### General guidelines:

- Did not save the notebook file prior to running the cell containing "export". We cannot see your output if you do not save before generating the zip file. (-10)
- Functions are defined more than once. (-10)
- Import statements are not all placed at the top of the notebook. (-10)
- Used concepts or modules not covered in class yet. (-10)

### Question specific guidelines:

- q1 (2)
	- `count` function is not used (-2)

- q2 (2)
	- `get_publisher` function is not used (-2)

- q3 (4)
	- `get_name` function is not used (-2)
	- index of the last game is hardcoded (-2)

- `format_price` (2)
	- function logic is incorrect (-2)

- q4 (4)
	- `get_price` function is not used (-1)
	- `format_price` function is not used (-1)
	- incorrect logic is used to answer (-1)

- q5 (4)
	- `format_price` function is not used (-1)
	- number of games in the dataset is hardcoded (-1)
	- did not exit loop and instead iterated further after finding the game (-2)

- q6 (4)
	- `format_price` function is not used (-1)
	- number of games in the dataset is hardcoded (-1)
	- incorrect logic is used to answer (-1)

- q7 (4)
	- number of games in the dataset is hardcoded (-1)
	- incorrect logic is used to answer (-1)

- `format_num_reviews` (3)
	- function output is incorrect when the number of reviews has suffix `K` (-1)
	- function output is incorrect when the number of reviews has suffix `M` (-1)
	- function output is incorrect when the number of reviews has no suffix (-1)

- q8 (4)
	- `format_num_reviews` function is not used (-1)
	- number of games in the dataset is hardcoded (-1)
	- did not exit loop and instead iterated further after finding the game (-2)

- q9 (4)
	- `format_num_reviews` function is not used (-1)
	- number of games in the dataset is hardcoded (-1)
	- incorrect logic is used to answer (-2)

- q10 (3)
	- the game with the highest number of negative reviews is recomputed (-1)
	- incorrect logic is used to answer (-1)

- `get_year` (2)
	- function logic is incorrect (-2)

- `get_month` (2)
	- function logic is incorrect (-2)

- `get_day` (2)
	- function logic is incorrect (-2)

- q11 (5)
	- number of games in the dataset is hardcoded (-1)
	- `get_month` function is not used to determine the month of release (-1)
	- `get_day` function is not used to determine the day of release (-1)
	- incorrect logic is used to answer (-1)

- q12 (5)
	- number of games in the dataset is hardcoded (-1)
	- correct comparison operator is not used (-1)
	- `get_year` function is not used to determine the year of release (-1)
	- variable to store the index or year of the earliest game is not initialized as `None` (-1)
	- used indices of the games to determine the earliest game (-1)

- q13 (5)
	- number of games in the dataset is hardcoded (-1)
	- `format_price` function is not used to convert the `price` to float (-1)
	- variable to store the index or year of the earliest game is not initialized as `None` (-1)
	- incorrect logic is used to answer (-1)

- q14 (5)
	- number of games in the dataset is hardcoded (-1)
	- `format_price` function is not used to convert the `price` to float (-1)
	- variable to store the index or year of the earliest game is not initialized as `None` (-1)
	- incorrect logic is used to answer (-1)

- `best_in_range` (6)
	- variable to store the index of the best game is not initialized as `None` (-2)
	- function does not consider all games released between `year1` and `year2` (-1)
	- number of games in the dataset is hardcoded (-1)
	- function logic is incorrect (-2)

- q15 (3)
	- `best_in_range` function is not used to determine the index of the best game (-2)
	- `get_name` function is not used (-1)

- q16 (4)
	- `best_in_range` function is not used to determine the index of the best game (-2)
	- `format_price` function is not used to determine the index of the best game (-1)
	- `get_price` function is not used (-1)

- q17 (4)
	- `best_in_range` function is not used to determine the index of the best game (-2)
	- `get_publisher` function is not used (-1)

- `get_year_total` (4)
	- function logic is incorrect (-2)
	- `get_year` function is not used to determine the year of release (-1)
	- number of games in the dataset is hardcoded (-1)

- q18 (3)
	- `get_year_total` function is not used to determine the number of games (-2)

- q19 (5)
	- `get_year_total` function is not used to determine the number of games (-2)
	- did not loop through the years in the 21st century (-2)
	- incorrect logic is used to answer (-1)

- q20 (5)
	- `best_in_range` function is not used to determine the index of the best game (-1)
	- `format_num_reviews` function is not used (-1)
	- incorrect logic is used to answer (-2)

