# Project 8 (P8): Going to the Movies


## Corrections and clarifications:

* None yet.

**Find any issues?** Report to us:

- Samuel Guo <sguo258@wisc.edu>
- David Parra <deparra@wisc.edu>

## Instructions:

This project will focus on **combining data from different csv files** and **creating useful data structures**. To start, download [`p8.ipynb`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/p8/p8.ipynb), [`public_tests.py`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/p8/public_tests.py), [`small_movies.csv`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/p8/small_movies.csv), [`small_mapping.csv`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/p8/small_mapping.csv), [`movies.csv`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/p8/movies.csv), and [`mapping.csv`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/p8/mapping.csv).

If it takes too long to load the files `movies.csv` and `mapping.csv` on GitLab, you can directly download the file from these link: [`movies.csv`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/raw/main/p8/movies.csv), and [`mapping.csv`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/raw/main/p8/mapping.csv). You will need to **Right Click**, and click on the **Save as...** button to save the file through this method.

**Note:** Please go through [Lab-P8](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/lab-p8) before you start the project. The lab contains some very important information that will be necessary for you to finish the project.

You will work on `p8.ipynb` and hand it in. You should follow the provided directions for each question. Questions have **specific** directions on what **to do** and what **not to do**.

------------------------------

## IMPORTANT Submission instructions:
- Review the [Grading Rubric](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-s24-projects/-/tree/main/p8/rubric.md), to ensure that you don't lose points during code review.
- Login to [Gradescope](https://www.gradescope.com/) and upload the zip file into the P8 assignment.
- If you completed the project with a **partner**, make sure to **add their name** by clicking "Add Group Member"
in Gradescope when uploading the P8 zip file.

   <img src="images/add_group_member.png" width="700">

   **Warning:** You will have to add your partner on Gradescope even if you have filled out this information in your `p8.ipynb` notebook.

- It is **your responsibility** to make sure that your project clears auto-grader tests on the Gradescope test system. Otter test results should be available within forty minutes after your submission (usually within ten minutes). **Ignore** the `-/100.00` that is displayed to the right. You should be able to see both PASS / FAIL results for the 20 test cases, which is accessible via Gradescope Dashboard (as in the image below):

    <img src="images/gradescope.png" width="700">

- You can view your **final score** at the **end of the page**. If you pass all tests, then you will receive **full points** for the project. Otherwise, you can see your final score in the **summary** section of the test results (as in the image below):

   <img src="images/summary.png" width="700">

   If you want more details on why you lost points on a particular test, you can scroll up to find more details about the test.
