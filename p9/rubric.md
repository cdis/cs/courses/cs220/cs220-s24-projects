# Project 9 (P9) Grading Rubric


## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- To ensure that you don't lose any points, you must **review** the rubric and make sure that you have followed the instructions provided in the project correctly.
- If you **fail** the **public tests** for a function or **hardcode** the answers to that question, you will automatically lose **all** points for that question.

## Rubric

### General guidelines:

- Outputs not visible/did not save the notebook file prior to running the cell containing "export". We cannot see your output if you do not save before generating the zip file. (-3)
- Used concepts/modules such as `csv.DictReader` and `pandas` not covered in class yet. Note that built-in functions that you have been introduced to can be used. (-3)
- Large outputs such as `movies` are displayed in the notebook. (-3)
- Import statements are not mentioned in the required cell at the top of the notebook. (-1)

### Question specific guidelines:

- q1 (3)
	- `find_specific_movies` function is not used (-1)
	- median calculation is incorrect (-1)
	- answer uses loops to iterate over dictionary keys (-1)

- q2 (4)
	- `find_specific_movies` function is not used (-1)
	- rating of highest rated *Toy Story* movie is hardcoded (-1)
	- did not find all the movies with the highest rating (-1)
	- answer uses loops to iterate over dictionary keys (-1)

- `bucketize` (5)
	- input or output of the function is of wrong type (-0)
	- function logic is incorrect when `category` data is stored in `movies` as a `str` or `int` (-2)
	- function logic is incorrect when `category` data is stored in `movies` as a `list` (-1)
	- categories are hardcoded while checking if their data is stored as a `list` or not (-1)
	- function is defined more than once (-1)

- `cast_buckets` (2)
	- `bucketize` function is not used (-1)
	- data structure is defined incorrectly (-1)

- `director_buckets` (2)
	- `bucketize` function is not used (-1)
	- data structure is defined incorrectly (-1)

- `genre_buckets` (2)
	- `bucketize` function is not used (-1)
	- data structure is defined incorrectly (-1)

- `year_buckets` (2)
	- `bucketize` function is not used (-1)
	- data structure is defined incorrectly (-1)

- q3 (2)
	- `cast_buckets` data structure is not used (-2)

- q4 (4)
	- incorrect logic is used to answer (-2)
	- plot is incorrect (-1)
	- plot is not properly labeled (-1)

- q5 (4)
	- incorrect logic is used to answer (-2)
	- plot is incorrect (-1)
	- plot is not properly labeled (-1)

- q6 (4)
	- incorrect logic is used to answer (-2)
	- plot is incorrect (-1)
	- plot is not properly labeled (-1)

- q7 (4)
	- incorrect logic is used to answer (-2)
	- plot is incorrect (-1)
	- plot is not properly labeled (-1)

- q8 (4)
	- incorrect logic is used to answer (-2)
	- recomputed variable defined in Question 7 (-1)

- q9 (4)
	- incorrect logic is used to answer (-2)
	- does not find only the movies directed by *Martin Scorsese* and starring *Robert De Niro* (-1)

- q10 (4)
	- movies released after the `year` *2020* are not excluded (-1)
	- incorrect comparison operators are used to check if the **number** of movies, **median**, and **minimum** `rating` are as expected (-1)
	- incorrect logic is used to answer (-1)

